# Introduction To JavaScript

<!-- MarkdownTOC -->

- [Overview](#overview)
	- [Why Learn JavaScript?](#why-learn-javascript)
	- [Take-Away Skills:](#take-away-skills)
- [Introduction](#introduction)
- [Conditionals](#conditionals)
- [Functions](#functions)
- [Scope](#scope)
- [Arrays](#arrays)
- [Loops](#loops)
- [Iterators](#iterators)
	- [Higher-Order Functions](#higher-order-functions)
		- [Introduction](#introduction-1)
		- [Functions as Data](#functions-as-data)
		- [Functions as Parameters](#functions-as-parameters)
		- [Review](#review)
	- [Iterators](#iterators-1)
		- [Introduction to Iterators](#introduction-to-iterators)
		- [The .forEach\(\) Method](#the-foreach-method)
		- [The .map\(\) Method](#the-map-method)
		- [The .filter\(\) Method](#the-filter-method)
		- [The .findIndex\(\) Method](#the-findindex-method)
		- [The .reduce\(\) Method](#the-reduce-method)
		- [Iterator Documentation](#iterator-documentation)
		- [Choose the Right Iterator](#choose-the-right-iterator)
		- [Review](#review-1)
- [Objects](#objects)
	- [Objects](#objects-1)
		- [Introduction to Objects](#introduction-to-objects)
		- [Creating Object Libraries](#creating-object-libraries)
		- [Accessing Properties](#accessing-properties)
		- [Bracket Notation](#bracket-notation)
		- [Property Assignment](#property-assignment)
		- [Methods](#methods)
		- [Nested Objects](#nested-objects)
		- [Pass By Reference](#pass-by-reference)
		- [Looping Throught Objects](#looping-throught-objects)
		- [Review](#review-2)
	- [Advanced Objects](#advanced-objects)
		- [Advanced Objects Introduction](#advanced-objects-introduction)
		- [The this Keyword](#the-this-keyword)
		- [Arrow Functions and this](#arrow-functions-and-this)
		- [Privacy](#privacy)
		- [Getters](#getters)
		- [Setters](#setters)
		- [Factory Functions](#factory-functions)
		- [Property Value Shorthand](#property-value-shorthand)
		- [Destructured Assignment](#destructured-assignment)
		- [Built-in Object Methods](#built-in-object-methods)
		- [Review](#review-3)
- [Classes](#classes)
	- [Introduction to Classes](#introduction-to-classes)
	- [Constructor](#constructor)
	- [Instance](#instance)
	- [Methods](#methods-1)
	- [Method Calls](#method-calls)
	- [Inheritance I](#inheritance-i)
	- [Inheritance II](#inheritance-ii)
	- [Inheritance III](#inheritance-iii)
	- [Inheritance IV](#inheritance-iv)
	- [Inheritance V](#inheritance-v)
	- [Static Methods](#static-methods)
	- [Review](#review-4)
- [Browser Compatibility and Transpilation](#browser-compatibility-and-transpilation)
	- [Intro](#intro)
	- [caniuse.com](#caniusecom)
	- [Why ES6?](#why-es6)
	- [Transplation With Babel](#transplation-with-babel)
	- [npm init](#npm-init)
	- [Instal Node Packages](#instal-node-packages)
	- [.babelrc](#babelrc)
	- [Babel Source Lib](#babel-source-lib)
	- [Build](#build)
	- [Review](#review-5)
- [Modules](#modules)
	- [Hello Modules](#hello-modules)
	- [module.exports](#moduleexports)
	- [require\(\)](#require)
	- [module.exports II](#moduleexports-ii)
	- [export default](#export-default)
	- [import](#import)
	- [Named Exports](#named-exports)
	- [Named Imports](#named-imports)
	- [Export Named Exports](#export-named-exports)
	- [Import Named Imports](#import-named-imports)
	- [Export as](#export-as)
	- [Import as](#import-as)
	- [Combining Export Statements](#combining-export-statements)
	- [Combining Import Statements](#combining-import-statements)
	- [Review](#review-6)
- [JavaScript Promises](#javascript-promises)
	- [Introduction](#introduction-2)
	- [What is a Promise?](#what-is-a-promise)
	- [Constructing a Promise Object](#constructing-a-promise-object)
	- [The Node setTimeout\(\) Function](#the-node-settimeout-function)
	- [Consuming Promises](#consuming-promises)
	- [The onFulfilled and onRejected Functions](#the-onfulfilled-and-onrejected-functions)
	- [Using catch\(\) with Promises](#using-catch-with-promises)
	- [Chaining Multiple Promises](#chaining-multiple-promises)
	- [Avoiding Common Mistakes](#avoiding-common-mistakes)
	- [Using Promise.all\(\)](#using-promiseall)
	- [Review](#review-7)
- [JavaScript Async-Await](#javascript-async-await)
	- [Introduction](#introduction-3)
	- [The async Keyword](#the-async-keyword)
	- [The await Operator](#the-await-operator)
	- [Writing async Functions](#writing-async-functions)
	- [Handling Dependent Promises](#handling-dependent-promises)
	- [Handling Errors](#handling-errors)
	- [Handling Independent Promises](#handling-independent-promises)
	- [Await Promise.all\(\)](#await-promiseall)
	- [Review](#review-8)
- [Requests](#requests)

<!-- /MarkdownTOC -->

<a id="overview"></a>
## Overview

Learn the JavaScript fundamentals you'll need for front-end or back-end development.

<a id="why-learn-javascript"></a>
### Why Learn JavaScript?
JavaScript is among the most powerful and flexible programming languages of the web. It powers the dynamic behavior on most websites, including this one.

<a id="take-away-skills"></a>
### Take-Away Skills:
You will learn programming fundamentals and basic object-oriented concepts using the latest JavaScript syntax. The concepts covered in these lessons lay the foundation for using JavaScript in any environment.

<a id="introduction"></a>
## Introduction

<a id="conditionals"></a>
## Conditionals

<a id="functions"></a>
## Functions

<a id="scope"></a>
## Scope

<a id="arrays"></a>
## Arrays

<a id="loops"></a>
## Loops

<a id="iterators"></a>
## Iterators

<a id="higher-order-functions"></a>
### Higher-Order Functions

<a id="introduction-1"></a>
#### Introduction

We are often unaware of the number of assumptions we make when we communicate with other people in our native languages. If we told you to “count to three,” we would expect you to say or think the numbers one, two and three. We assumed you would know to start with “one” and end with “three”. With programming, we’re faced with needing to be more explicit with our directions to the computer. Here’s how we might tell the computer to “count to three”:

```js
for (let i = 1; i<=3; i++) {
  console.log(i)
}
```

When we speak to other humans, we share a vocabulary that gives us quick ways to communicate complicated concepts. When we say “bake”, it calls to mind a familiar subroutine — preheating an oven, putting something into an oven for a set amount of time, and finally removing it. This allows us to *abstract* away a lot of the details and communicate key concepts more concisely. Instead of listing all those details, we can say, “We baked a cake,” and still impart all that meaning to you.

In programming, we can accomplish “abstraction” by writing functions. In addition to allowing us to reuse our code, functions help to make clear, readable programs. If you encountered `countToThree()` in a program, you might be able to quickly guess what the function did without having to stop and read the function’s body.

We’re also going to learn about another way to add a level of abstraction to our programming: *higher-order functions*. *Higher-order functions* are functions that accept other functions as arguments and/or return functions as output. This enables us to build abstractions on other abstractions, just like “We hosted a birthday party” is an abstraction that may build on the abstraction “We made a cake.”

In summary, using more abstraction in our code allows us to write more modular code which is easier to read and debug.

<a id="functions-as-data"></a>
#### Functions as Data

JavaScript functions behave like any other data type in the language; we can assign functions to variables, and we can reassign them to new variables.

Below, we have an annoyingly long function name that hurts the readability of any code in which it’s used. Let’s pretend this function does important work and needs to be called repeatedly!

```js
const announceThatIAmDoingImportantWork = () => {
    console.log("I’m doing very important work!");
};
```

What if we wanted to rename this function without sacrificing the source code? We can re-assign the function to a variable with a suitably short name:

```js
const busy = announceThatIAmDoingImportantWork;

busy(); // This function call barely takes any space!
```

`busy` is a variable that holds a *reference* to our original function. If we could look up the address in memory of `busy` and the address in memory of `announceThatIAmDoingImportantWork` they would point to the same place. Our new `busy()` function can be invoked with parentheses as if that was the name we originally gave our function.

Notice how we assign `announceThatIAmDoingImportantWork` without parentheses as the value to the `busy` variable. We want to assign the value of the function itself, not the value it returns when invoked.

In JavaScript, functions are *first class objects*. This means that, like other objects you’ve encountered, JavaScript functions can have properties and methods.

Since functions are a type of object, they have properties such as `.length` and `.name` and methods such as `.toString()`. You can see more about the methods and properties of functions [in the documentation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function).

Functions are special because we can invoke them, but we can still treat them like any other type of data. Let’s get some practice doing that!

<a id="functions-as-parameters"></a>
#### Functions as Parameters

Since functions can behave like any other type of data in JavaScript, it might not surprise you to learn that we can also pass functions (into other functions) as parameters. A *higher-order function* is a function that either accepts functions as parameters, returns a function, or both! We call the functions that get passed in as parameters and invoked callback functions because they get called during the execution of the higher-order function.

When we pass a function in as an argument to another function, we don’t invoke it. Invoking the function would evaluate to the return value of that function call. With callbacks, we pass in the function itself by typing the function name *without* the parentheses (that would evaluate to the result of calling the function):

```js
const timeFuncRuntime = funcParameter => {
   let t1 = Date.now();
   funcParameter();
   let t2 = Date.now();
   return t2 - t1;
}

const addOneToOne = () => 1 + 1;

timeFuncRuntime(addOneToOne);
```

We wrote a higher-order function, `timeFuncRuntime()`. It takes in a function as an argument, saves a starting time, invokes the callback function, records the time after the function was called, and returns the time the function took to run by subtracting the starting time from the ending time.

This higher-order function could be used with any callback function which makes it a potentially powerful piece of code.

We then invoked `timeFuncRuntime()` first with the `addOneToOne()` function - note how we passed in addOneToOne and did not invoke it.

```js
timeFuncRuntime(() => {
  for (let i = 10; i>0; i--){
    console.log(i);
  }
});
```

In this example, we invoked `timeFuncRuntime()` with an anonymous function that counts backwards from 10. Anonymous functions can be arguments too!

Let’s get some practice using functions and writing higher-order functions.

<a id="review"></a>
#### Review

Great job! By thinking about functions as data and learning about higher-order functions, you’ve taken important steps in being able to write clean, modular code and take advantage of JavaScript’s flexibility.

Let’s review what we learned in this lesson:

- Abstraction allows us to write complicated code in a way that’s easy to reuse, debug, and understand for human readers

- We can work with functions the same way we would any other type of data including reassigning them to new variables

- JavaScript functions are first-class objects, so they have properties and methods like any object

- Functions can be passed into other functions as parameters

- A higher-order function is a function that either accepts functions as parameters, returns a function, or both

<a id="iterators-1"></a>
### Iterators

<a id="introduction-to-iterators"></a>
#### Introduction to Iterators

Imagine you had a grocery list and you wanted to know what each item on the list was. You’d have to scan through each row and check for the item. This common task is similar to what we have to do when we want to iterate over, or loop through, an array. One tool at our disposal is the `for` loop. However, we also have access to built-in array methods which make looping easier.

The built-in JavaScript array methods that help us iterate are called *iteration methods*, at times referred to as iterators. Iterators are methods called on arrays to manipulate elements and return values.

In this lesson, you will learn the syntax for these methods, their return values, how to use the documentation to understand them, and how to choose the right iterator method for a given task.

<a id="the-foreach-method"></a>
#### The .forEach() Method

The first iteration method that we’re going to learn is `.forEach()`. Aptly named, `.forEach()` will execute the same code for each element of an array.
Diagram outlining the parts of an array iterator including the array identifier, the section that is the iterator, and the callback function

The code above will log a nicely formatted list of the groceries to the console. Let’s explore the syntax of invoking `.forEach()`.

- `groceries.forEach()` calls the `forEach` method on the groceries array.

- `.forEach()` takes an argument of callback function. Remember, a callback function is a function passed as an argument into another function.

- `.forEach()` loops through the array and executes the callback function for each element. During each execution, the current element is passed as an argument to the callback function.

- The return value for `.forEach()` will always be `undefined`.

Another way to pass a callback for `.forEach()` is to use arrow function syntax.

```js
groceries.forEach(groceryItem => console.log(groceryItem));
```

We can also define a function beforehand to be used as the callback function.

```js
function printGrocery(element){
  console.log(element);
}

groceries.forEach(printGrocery);
```

The above example uses a function declaration but you can also use a function expression or arrow function as well.

All three code snippets do the same thing. In each array iteration method, we can use any of the three examples to supply a callback function as an argument to the iterator. It’s good to be aware of the different ways to pass in callback functions as arguments in iterators because developers have different stylistic preferences. Nonetheless, due to the strong adoption of ES6, we will be using arrow function syntax in the later exercises.

<a id="the-map-method"></a>
#### The .map() Method

The second iterator we’re going to cover is `.map()`. When `.map()` is called on an array, it takes an argument of a callback function and returns a new array! Take a look at an example of calling `.map()`:

```js
const numbers = [1, 2, 3, 4, 5]; 

const bigNumbers = numbers.map(number => {
  return number * 10;
});
```

`.map()` works in a similar manner to `.forEach()` — the major difference is that `.map()` returns a new array.

In the example above:

- `numbers` is an array of numbers.

- `bigNumbers` will store the return value of calling `.map()` on `numbers`.

- `numbers.map` will iterate through each element in the `numbers` array and pass the element into the callback function.

- `return number * 10` is the code we wish to execute upon each element in the array. This will save each value from the `numbers` array, multiplied by `10`, to a new array.

If we take a look at `numbers` and `bigNumbers`:

```js
console.log(numbers); // Output: [1, 2, 3, 4, 5]
console.log(bigNumbers); // Output: [10, 20, 30, 40, 50]
```

Notice that the elements in `numbers` were not altered and `bigNumbers` is a new array.

<a id="the-filter-method"></a>
#### The .filter() Method

Another useful iterator method is `.filter()`. Like `.map()`, `.filter()` returns a new array. However, `.filter()` returns an array of elements after filtering out certain elements from the original array. The callback function for the `.filter()` method should return `true` or `false` depending on the element that is passed to it. The elements that cause the callback function to return `true` are added to the new array. Take a look at the following example:

```js
const words = ['chair', 'music', 'pillow', 'brick', 'pen', 'door']; 

const shortWords = words.filter(word => {
  return word.length < 6;
});
```

- `words` is an array that contains string elements.

- `const shortWords =` declares a new variable that will store the returned array from invoking `.filter()`.

- The callback function is an arrow function has a single parameter, `word`. Each element in the `words` array will be passed to this function as an argument.

- `word.length < 6;` is the condition in the callback function. Any `word` from the `words` array that has fewer than `6` characters will be added to the `shortWords` array.

Let’s also check the values of `words` and `shortWords`:

```js
console.log(words); // Output: ['chair', 'music', 'pillow', 'brick', 'pen', 'door']; 
console.log(shortWords); // Output: ['chair', 'music', 'brick', 'pen', 'door']
```

Observe how `words` was not mutated, i.e. changed, and `shortWords` is a new array.

<a id="the-findindex-method"></a>
#### The .findIndex() Method

We sometimes want to find the location of an element in an array. That’s where the `.findIndex()` method comes in! Calling `.findIndex()` on an array will return the index of the first element that evaluates to `true` in the callback function.

```js
const jumbledNums = [123, 25, 78, 5, 9]; 

const lessThanTen = jumbledNums.findIndex(num => {
  return num < 10;
});
```

- `jumbledNums` is an array that contains elements that are numbers.

- `const lessThanTen =` declares a new variable that stores the returned index number from invoking `.findIndex()`.

- The callback function is an arrow function has a single parameter, `num`. Each element in the `jumbledNums` array will be passed to this function as an argument.

- `num < 10;` is the condition that elements are checked against. `.findIndex()` will return the index of the first element which evaluates to `true` for that condition.

Let’s take a look at what `lessThanTen` evaluates to:

```js
console.log(lessThanTen); // Output: 3 
```

If we check what element has index of 3:

```js
console.log(jumbledNums[3]); // Output: 5
```

Great, the element in index `3` is the number `5`. This makes sense since `5` is the first element that is less than 10.

If there isn’t a single element in the array that satisfies the condition in the callback, then `.findIndex()` will return `-1`.

```js
const greaterThan1000 = jumbledNums.findIndex(num => {
  return num > 1000;
});

console.log(greaterThan1000); // Output: -1
```

<a id="the-reduce-method"></a>
#### The .reduce() Method

Another widely used iteration method is `.reduce()`. The `.reduce()` method returns a single value after iterating through the elements of an array, thereby *reducing* the array. Take a look at the example below:

```js
const numbers = [1, 2, 4, 10];

const summedNums = numbers.reduce((accumulator, currentValue) => {
  return accumulator + currentValue
})

console.log(summedNums) // Output: 17
```

Here are the values of `accumulator` and `currentValue` as we iterate through the `numbers` array:

Iteration | `accumulator` | `currentValue` | return value
--- | --- | --- | ---
First | 1 | 2 | 3
Second | 3 | 4 | 7
Third | 7 | 10 | 17

Now let’s go over the use of `.reduce()` from the example above:

- `numbers` is an array that contains numbers.

- `summedNums` is a variable that stores the returned value of invoking `.reduce()` on `numbers`.

- `numbers.reduce()` calls the `.reduce()` method on the `numbers` array and takes in a callback function as argument.

- The callback function has two parameters, `accumulator` and `currentValue`. The value of `accumulator` starts off as the value of the first element in the array and the `currentValue` starts as the second element. To see the value of `accumulator` and `currentValue` change, review the chart above.

- As `.reduce()` iterates through the array, the return value of the callback function becomes the `accumulator` value for the next iteration, `currentValue` takes on the value of the current element in the looping process.

The `.reduce()` method can also take an optional second parameter to set an initial value for `accumulator` (remember, the first argument is the callback function!). For instance:

```js
const numbers = [1, 2, 4, 10];

const summedNums = numbers.reduce((accumulator, currentValue) => {
  return accumulator + currentValue
}, 100)  // <- Second argument for .reduce()

console.log(summedNums); // Output: 117
```

Here’s an updated chart that accounts for the second argument of `100`:

Iteration | `accumulator` | `currentValue` | return value
--- | --- | --- | ---
First | 100 | 1 | 101
Second | 101 | 2 | 103
Third | 103 | 4 | 107
Fourth | 107 | 10 | 117


<a id="iterator-documentation"></a>
#### Iterator Documentation

There are many additional built-in array methods, a complete list of which is on the [MDN’s Array iteration methods page](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#Iteration_methods).

The documentation for each method contains several sections:

1. A short definition.

1. A block with the correct syntax for using the method.

1. A list of parameters the method accepts or requires.

1. The return value of the function.

1. An extended description.

1. Examples of the method’s use.

1. Other additional information.

In the instructions below, there are some errors in the code. Use the documentation for a given method to determine the error or fill in a blank to make the code run correctly.

<a id="choose-the-right-iterator"></a>
#### Choose the Right Iterator

There are many iteration methods you can choose. In addition to learning the correct syntax for the use of iteration methods, it is also important to learn how to choose the correct method for different scenarios. The exercises below will give you the opportunity to do just that!

You’ll see errors pop up in the terminal as you work through this exercise, but by the end the errors will be gone!

<a id="review-1"></a>
#### Review

Awesome job on clearing the iterators lesson! You have learned a number of useful methods in this lesson as well as how to use the JavaScript documentation from the Mozilla Developer Network to discover and understand additional methods. Let’s review!

- `.forEach()` is used to execute the same code on every element in an array but does not change the array and returns `undefined`.

- `.map()` executes the same code on every element in an array and returns a new array with the updated elements.

- `.filter()` checks every element in an array to see if it meets certain criteria and returns a new array with the elements that return truthy for the criteria.

- `.findIndex()` returns the index of the first element of an array which satisfies a condition in the callback function. It returns `-1` if none of the elements in the array satisfies the condition.

- `.reduce()` iterates through an array and takes the values of the elements and returns a single value.

- All iterator methods takes a callback function that can be pre-defined, or a function expression, or an arrow function.

- You can visit the [Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array) to learn more about iterator methods (and all other parts of JavaScript!).

<a id="objects"></a>
## Objects

<a id="objects-1"></a>
### Objects

<a id="introduction-to-objects"></a>
#### Introduction to Objects

It’s time to learn more about the basic structure that permeates nearly every aspect of JavaScript programming: objects.

You’re probably already more comfortable with objects than you think, because JavaScript loves objects! Many components of the language are actually objects under the hood, and even the parts that aren’t— like strings or numbers— can still act like objects in some instances.

There are only seven fundamental data types in JavaScript, and six of those are the primitive data types: string, number, boolean, null, undefined, and symbol. With the seventh type, objects, we open our code to more complex possibilities. We can use JavaScript objects to model real-world things, like a basketball, or we can use objects to build the data structures that make the web possible.

At their core, JavaScript objects are containers storing related data and functionality, but that deceptively simple task is extremely powerful in practice. You’ve been using the power of objects all along, but now it’s time to understand the mechanics of objects and start making your own!

<a id="creating-object-libraries"></a>
#### Creating Object Libraries

Objects can be assigned to variables just like any JavaScript type. We use curly braces, `{}`, to designate an *object literal*:

```js
let spaceship = {}; // spaceship is an empty object
```

We fill an object with unordered data. This data is organized into *key-value pairs*. A key is like a variable name that points to a location in memory that holds a value.

A key’s value can be of any data type in the language including functions or other objects.

We make a key-value pair by writing the key’s name, or *identifier*, followed by a colon and then the value. We separate each key-value pair in an object literal with a comma (`,`). Keys are strings, but when we have a key that does not have any special characters in it, JavaScript allows us to omit the quotation marks:

```js
// An object literal with two key-value pairs
let spaceship = {
  'Fuel Type': 'diesel',
  color: 'silver'
};
```

The `spaceship` object has two properties `Fuel Type` and `color`. `'Fuel Type'` has quotation marks because it contains a space character.

Let’s make some objects!

<a id="accessing-properties"></a>
#### Accessing Properties

There are two ways we can access an object’s property. Let’s explore the first way— dot notation, `.`.

You’ve used dot notation to access the properties and methods of built-in objects and data instances:

```js
'hello'.length; // Returns 5
```

With property dot notation, we write the object’s name, followed by the dot operator and then the property name (key):

```js
let spaceship = {
  homePlanet: 'Earth',
  color: 'silver'
};
spaceship.homePlanet; // Returns 'Earth',
spaceship.color; // Returns 'silver',
```

If we try to access a property that does not exist on that object, `undefined` will be returned.

```js
spaceship.favoriteIcecream; // Returns undefined
```

Let’s get some more practice using dot notation on an object!

<a id="bracket-notation"></a>
#### Bracket Notation

The second way to access a key’s value is by using bracket notation, `[ ]`.

You’ve used bracket notation when indexing an array:

```js
['A', 'B', 'C'][0]; // Returns 'A'
```

To use bracket notation to access an object’s property, we pass in the property name (key) as a string.

We **must** use bracket notation when accessing keys that have numbers, spaces, or special characters in them. Without bracket notation in these situations, our code would throw an error.

```js
let spaceship = {
  'Fuel Type': 'Turbo Fuel',
  'Active Duty': true,
  homePlanet: 'Earth',
  numCrew: 5
};
spaceship['Active Duty'];   // Returns true
spaceship['Fuel Type'];   // Returns  'Turbo Fuel'
spaceship['numCrew'];   // Returns 5
spaceship['!!!!!!!!!!!!!!!'];   // Returns undefined
```

With bracket notation you can also use a variable inside the brackets to select the keys of an object. This can be especially helpful when working with functions:

```js
let returnAnyProp = (objectName, propName) => objectName[propName];

returnAnyProp(spaceship, 'homePlanet'); // Returns 'Earth'
```

If we tried to write our `returnAnyProp()` function with dot notation (`objectName.propName`) the computer would look for a key of 'propName' on our object and not the value of the `propName` parameter.

Let’s get some practice using bracket notation to access properties!

<a id="property-assignment"></a>
#### Property Assignment

Once we’ve defined an object, we’re not stuck with all the properties we wrote. Objects are *mutable* meaning we can update them after we create them!

We can use either dot notation, `.`, or bracket notation, `[]`, and the assignment operator, `=` to add new key-value pairs to an object or change an existing property.

One of two things can happen with property assignment:

- If the property already exists on the object, whatever value it held before will be replaced with the newly assigned value.

- If there was no property with that name, a new property will be added to the object.

It’s important to know that although we can’t reassign an object declared with `const`, we can still mutate it, meaning we can add new properties and change the properties that are there.

```js
const spaceship = {type: 'shuttle'};
spaceship = {type: 'alien'}; // TypeError: Assignment to constant variable.
spaceship.type = 'alien'; // Changes the value of the type property
spaceship.speed = 'Mach 5'; // Creates a new key of 'speed' with a value of 'Mach 5'
```

You can delete a property from an object with the delete operator.

```js
const spaceship = {
  'Fuel Type': 'Turbo Fuel',
  homePlanet: 'Earth',
  mission: 'Explore the universe' 
};

delete spaceship.mission;  // Removes the mission property
```

<a id="methods"></a>
#### Methods

When the data stored on an object is a function we call that a method. A property is what an object has, while a method is what an object does.

Do object methods seem familiar? That’s because you’ve been using them all along! For example `console` is a global javascript object and `.log()` is a method on that object. Math is also a global javascript object and `.floor()` is a method on it.

We can include methods in our object literals by creating ordinary, comma-separated key-value pairs. The key serves as our method’s name, while the value is an anonymous function expression.

```js
const alienShip = {
  invade: function () { 
    console.log('Hello! We have come to dominate your planet. Instead of Earth, it shall be called New Xaculon.')
  }
};
```

With the new method syntax introduced in ES6 we can omit the colon and the `function` keyword.

```js
const alienShip = {
  invade () { 
    console.log('Hello! We have come to dominate your planet. Instead of Earth, it shall be called New Xaculon.')
  }
};
```

Object methods are invoked by appending the object’s name with the dot operator followed by the method name and parentheses:

```js
alienShip.invade(); // Prints 'Hello! We have come to dominate your planet. Instead of Earth, it shall be called New Xaculon.'
```

<a id="nested-objects"></a>
#### Nested Objects

In application code, objects are often nested— an object might have another object as a property which in turn could have a property that’s an array of even more objects!

In our `spaceship` object, we want a `crew` object. This will contain all the crew members who do important work on the craft. Each of those `crew` members are objects themselves. They have properties like `name`, and `degree`, and they each have unique methods based on their roles. We can also nest other objects in the `spaceship` such as a `telescope` or nest details about the spaceship’s computers inside a parent `nanoelectronics` object.

```js
const spaceship = {
     telescope: {
        yearBuilt: 2018,
        model: '91031-XLT',
        focalLength: 2032 
     },
    crew: {
        captain: { 
            name: 'Sandra', 
            degree: 'Computer Engineering', 
            encourageTeam() { console.log('We got this!') } 
         }
    },
    engine: {
        model: 'Nimbus2000'
     },
     nanoelectronics: {
         computer: {
            terabytes: 100,
            monitors: 'HD'
         },
        'back-up': {
           battery: 'Lithium',
           terabytes: 50
         }
    }
}; 
```

We can chain operators to access nested properties. We’ll have to pay attention to which operator makes sense to use in each layer. It can be helpful to pretend you are the computer and evaluate each expression from left to right so that each operation starts to feel a little more manageable.

```js
spaceship.nanoelectronics['back-up'].battery; // Returns 'Lithium'
```

In the preceding code:

- First the computer evaluates `spaceship.nanoelectronics`, which results in an object containing the `back-up` and `computer` objects.

- We accessed the `back-up` object by appending `['back-up']`.

- The `back-up` object has a `battery` property, accessed with `.battery` which returned the value stored there: `'Lithium'`

<a id="pass-by-reference"></a>
#### Pass By Reference

Objects are *passed by reference*. This means when we pass a variable assigned to an object into a function as an argument, the computer interprets the parameter name as pointing to the space in memory holding that object. As a result, functions which change object properties actually mutate the object permanently (even when the object is assigned to a `const` variable).

```js
const spaceship = {
  homePlanet : 'Earth',
  color : 'silver'
};

let paintIt = obj => {
  obj.color = 'glorious gold'
};

paintIt(spaceship);

spaceship.color // Returns 'glorious gold'
```

Our function `paintIt()` permanently changed the color of our `spaceship` object. However, reassignment of the `spaceship` variable wouldn’t work in the same way:

```js
let spaceship = {
  homePlanet : 'Earth',
  color : 'red'
};
let tryReassignment = obj => {
  obj = {
    identified : false, 
    'transport type' : 'flying'
  }
  console.log(obj) // Prints {'identified': false, 'transport type': 'flying'}

};
tryReassignment(spaceship) // The attempt at reassignment does not work.
spaceship // Still returns {homePlanet : 'Earth', color : 'red'};

spaceship = {
  identified : false, 
  'transport type': 'flying'
}; // Regular reassignment still works.
```

Let’s look at what happened in the code example:

- We declared this `spaceship` object with `let`. This allowed us to reassign it to a new object with `identified` and `'transport type'` properties with no problems.

- When we tried the same thing using a function designed to reassign the object passed into it, the reassignment didn’t stick (even though calling `console.log()` on the object produced the expected result).

- When we passed `spaceship` into that function, `obj` became a reference to the memory location of the `spaceship` object, but *not* to the `spaceship` variable. This is because the `obj` parameter of the `tryReassignment()` function is a variable in its own right. The body of `tryReassignment()` has no knowledge of the `spaceship` variable at all!

- When we did the reassignment in the body of `tryReassignment()`, the `obj` variable came to refer to the memory location of the object `{'identified' : false, 'transport type' : 'flying'}`, while the `spaceship` variable was completely unchanged from its earlier value.

<a id="looping-throught-objects"></a>
#### Looping Throught Objects

Loops are programming tools that repeat a block of code until a condition is met. We learned how to iterate through arrays using their numerical indexing, but the key-value pairs in objects aren’t ordered! [JavaScript has given us alternative solution for iterating through objects with the `for...in` syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in).

`for...in` will execute a given block of code for each property in an object.

```js
let spaceship = {
    crew: {
    captain: { 
        name: 'Lily', 
        degree: 'Computer Engineering', 
        cheerTeam() { console.log('You got this!') } 
        },
    'chief officer': { 
        name: 'Dan', 
        degree: 'Aerospace Engineering', 
        agree() { console.log('I agree, captain!') } 
        },
    medic: { 
        name: 'Clementine', 
        degree: 'Physics', 
        announce() { console.log(`Jets on!`) } },
    translator: {
        name: 'Shauna', 
        degree: 'Conservation Science', 
        powerFuel() { console.log('The tank is full!') } 
        }
    }
}; 
// for...in
for (let crewMember in spaceship.crew) {
  console.log(`${crewMember}: ${spaceship.crew[crewMember].name}`)
};
```

Our `for...in` will iterate through each element of the `spaceship.crew` object. In each iteration, the variable `crewMember` is set to one of `spaceship.crew`‘s keys, enabling us to log a list of crew members’ role and `name`.

<a id="review-2"></a>
#### Review

Way to go! You’re well on your way to understanding the mechanics of objects in JavaScript. By building your own objects, you will have a better understanding of how JavaScript built-in objects work as well. You can also start imagining organizing your code into objects and modeling real world things in code.

Let’s review what we learned in this lesson:

- Objects store collections of *key-value* pairs.

- Each key-value pair is a property—when a property is a function it is known as a method.

- An object literal is composed of comma-separated key-value pairs surrounded by curly braces.

- You can access, add or edit a property within an object by using dot notation or bracket notation.

- We can add methods to our object literals using key-value syntax with anonymous function expressions as values or by using the new ES6 method syntax.

- We can navigate complex, nested objects by chaining operators.

- Objects are mutable—we can change their properties even when they’re declared with `const`.

- Objects are passed by reference — when we make changes to an object passed into a function, those changes are permanent.

- We can iterate through objects using the `For...in` syntax.

<a id="advanced-objects"></a>
### Advanced Objects

<a id="advanced-objects-introduction"></a>
#### Advanced Objects Introduction

Remember, objects in JavaScript are containers that store data and functionality. In this lesson, we will build upon the fundamentals of creating objects and explore some advanced concepts.

So if there are no objections, let’s learn more about objects!

In this lesson we will cover these topics:

- how to use the `this` keyword.

- conveying privacy in JavaScript methods.

- defining getters and setters in objects.

- creating factory functions.

- using destructuring techniques.

<a id="the-this-keyword"></a>
#### The this Keyword

Objects are collections of related data and functionality. We store that functionality in methods on our objects:

```js
const goat = {
  dietType: 'herbivore',
  makeSound() {
    console.log('baaa');
  }
};
```

In our `goat` object we have a `.makeSound()` method. We can invoke the `.makeSound()` method on `goat`.

```js
goat.makeSound(); // Prints baaa
```

Nice, we have a `goat` object that can print `baaa` to the console. Everything seems to be working fine. What if we wanted to add a new method to our `goat` object called `.diet()` that prints the `goat`‘s `dietType`?

```js
const goat = {
  dietType: 'herbivore',
  makeSound() {
    console.log('baaa');
  },
  diet() {
    console.log(dietType);
  }
};
goat.diet(); 
// Output will be "ReferenceError: dietType is not defined"
```

That’s strange, why is `dietType` not defined even though it’s a property of `goat`? That’s because inside the scope of the `.diet()` method, we don’t automatically have access to other properties of the `goat` object.

Here’s where the `this` keyword comes to the rescue. If we change the `.diet()` method to use the `this`, the `.diet()` works! :

```js
const goat = {
  dietType: 'herbivore',
  makeSound() {
    console.log('baaa');
  },
  diet() {
    console.log(this.dietType);
  }
};

goat.diet(); 
// Output: herbivore
```

The `this` keyword references the *calling* object which provides access to the calling object’s properties. In the example above, the calling object is `goat` and by using `this` we’re accessing the `goat` object itself, and then the `dietType` property of `goat` by using property dot notation.

Let’s get comfortable using the `this` keyword in a method.

<a id="arrow-functions-and-this"></a>
#### Arrow Functions and this

We saw in the previous exercise that for a method, the calling object is the object the method belongs to. If we use the `this` keyword in a method then the value of `this` is the calling object. However, it becomes a bit more complicated when we start using arrow functions for methods. Take a look at the example below:

```js
const goat = {
  dietType: 'herbivore',
  makeSound() {
    console.log('baaa');
  },
  diet: () => {
    console.log(this.dietType);
  }
};

goat.diet(); // Prints undefined
```

In the comment, you can see that `goat.diet()` would log `undefined`. So what happened? Notice that in the `.diet()` is defined using an arrow function.

Arrow functions inherently *bind*, or tie, an already defined `this` value to the function itself that is NOT the calling object. In the code snippet above, the value of `this` is the *global object*, or an object that exists in the global scope, which doesn’t have a `dietType` property and therefore returns `undefined`.

To read more about either arrow functions or the global object check out the MDN documentation of the [global object](https://developer.mozilla.org/en-US/docs/Glossary/Global_object) and [arrow functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions).

The key takeaway from the example above is to *avoid* using arrow functions when using `this` in a method!

<a id="privacy"></a>
#### Privacy

Accessing and updating properties is fundamental in working with objects. However, there are cases in which we don’t want other code simply accessing and updating an object’s properties. When discussing *privacy* in objects, we define it as the idea that only certain properties should be mutable or able to change in value.

Certain languages have privacy built-in for objects, but JavaScript does not have this feature. Rather, JavaScript developers follow naming conventions that signal to other developers how to interact with a property. One common convention is to place an underscore `_` before the name of a property to mean that the property should not be altered. Here’s an example of using `_` to prepend a property.

```js
const bankAccount = {
  _amount: 1000
}
```

In the example above, the `_amount` is not intended to be directly manipulated.

Even so, it is still possible to reassign `_amount`:

```js
bankAccount._amount = 1000000;
```

In later exercises, we’ll cover the use of methods called *getters* and *setters*. Both methods are used to respect the intention of properties prepended, or began, with `_`. Getters can return the value of internal properties and setters can safely reassign property values. For now, let’s see what happens if we can change properties that don’t have setters or getters.

<a id="getters"></a>
#### Getters

*Getters* are methods that get and return the internal properties of an object. But they can do more than just retrieve the value of a property! Let’s take a look at a getter method:

```js
const person = {
  _firstName: 'John',
  _lastName: 'Doe',
  get fullName() {
    if (this._firstName && this._lastName){
      return `${this._firstName} ${this._lastName}`;
    } else {
      return 'Missing a first name or a last name.';
    }
  }
}

// To call the getter method: 
person.fullName; // 'John Doe'
```

Notice that in the getter method above:

- We use the `get` keyword followed by a function.

- We use an `if...else` conditional to check if both `_firstName` and `_lastName` exist (by making sure they both return truthy values) and then return a different value depending on the result.

- We can access the calling object’s internal properties using `this`. In `fullName`, we’re accessing both `this._firstName` and `this._lastName`.

- In the last line we call `fullName` on `person`. In general, getter methods do not need to be called with a set of parentheses. Syntactically, it looks like we’re accessing a property.

Now that we’ve gone over syntax, let’s discuss some notable advantages of using getter methods:

- Getters can perform an action on the data when getting a property.

- Getters can return different values using conditionals.

- In a getter, we can access the properties of the calling object using `this`.

- The functionality of our code is easier for other developers to understand.

Another thing to keep in mind when using getter (and setter) methods is that properties cannot share the same name as the getter/setter function. If we do so, then calling the method will result in an infinite call stack error. One workaround is to add an underscore before the property name like we did in the example above.

Great, let’s go getter!

<a id="setters"></a>
#### Setters

Along with getter methods, we can also create *setter* methods which reassign values of existing properties within an object. Let’s see an example of a setter method:

```js
const person = {
  _age: 37,
  set age(newAge){
    if (typeof newAge === 'number'){
      this._age = newAge;
    } else {
      console.log('You must assign a number to age');
    }
  }
};
```

Notice that in the example above:

- We can perform a check for what value is being assigned to `this._age`.

- When we use the setter method, only values that are numbers will reassign `this._age`

- There are different outputs depending on what values are used to reassign `this._age`.

Then to use the setter method:

```js
person.age = 40;
console.log(person._age); // Logs: 40
person.age = '40'; // Logs: You must assign a number to age
```

Setter methods like `age` do not need to be called with a set of parentheses. Syntactically, it looks like we’re reassigning the value of a property.

Like getter methods, there are similar advantages to using setter methods that include checking input, performing actions on properties, and displaying a clear intention for how the object is supposed to be used. Nonetheless, even with a setter method, it is still possible to directly reassign properties. For example, in the example above, we can still set `._age` directly:

```js
person._age = 'forty-five'
console.log(person._age); // Prints forty-five
```

<a id="factory-functions"></a>
#### Factory Functions

So far we’ve been creating objects individually, but there are times where we want to create many instances of an object quickly. Here’s where *factory functions* come in. A real world factory manufactures multiple copies of an item quickly and on a massive scale. A factory function is a function that returns an object and can be reused to make multiple object instances. Factory functions can also have parameters allowing us to customize the object that gets returned.

Let’s say we wanted to create an object to represent monsters in JavaScript. There are many different types of monsters and we could go about making each monster individually but we can also use a factory function to make our lives easier. To achieve this diabolical plan of creating multiple monsters objects, we can use a factory function that has parameters:

```js
const monsterFactory = (name, age, energySource, catchPhrase) => {
  return { 
    name: name,
    age: age, 
    energySource: energySource,
    scare() {
      console.log(catchPhrase);
    } 
  }
};
```

In the `monsterFactory` function above, it has four parameters and returns an object that has the properties: `name`, `age`, `energySource`, and `scare()`. To make an object that represents a specific monster like a ghost, we can call `monsterFactory` with the necessary arguments and assign the return value to a variable:

```js
const ghost = monsterFactory('Ghouly', 251, 'ectoplasm', 'BOO!');
ghost.scare(); // 'BOO!'
```

Now we have a `ghost` object as a result of calling `monsterFactory()` with the needed arguments. With `monsterFactory` in place, we don’t have to create an object literal every time we need a new monster. Instead, we can invoke the `monsterFactory` function with the necessary arguments to ~~take over the world~~ make a monster for us!

<a id="property-value-shorthand"></a>
#### Property Value Shorthand

ES6 introduced some new shortcuts for assigning properties to variables known as *destructuring*.

In the previous exercise, we created a factory function that helped us create objects. We had to assign each property a key and value even though the key name was the same as the parameter name we assigned to it. To remind ourselves, here’s a truncated version of the factory function:

```js
const monsterFactory = (name, age) => {
  return { 
    name: name,
    age: age
  }
};
```

Imagine if we had to include more properties, that process would quickly become tedious! But we can use a destructuring technique, called *property value shorthand*, to save ourselves some keystrokes. The example below works exactly like the example above:

```js
const monsterFactory = (name, age) => {
  return { 
    name,
    age 
  }
};
```

Notice that we don’t have to repeat ourselves for property assignments!

<a id="destructured-assignment"></a>
#### Destructured Assignment

We often want to extract key-value pairs from objects and save them as variables. Take for example the following object:

```js
const vampire = {
  name: 'Dracula',
  residence: 'Transylvania',
  preferences: {
    day: 'stay inside',
    night: 'satisfy appetite'
  }
};
```

If we wanted to extract the `residence` property as a variable, we could using the following code:

```js
const residence = vampire.residence; 
console.log(residence); // Prints 'Transylvania' 
```

However, we can also take advantage of a destructuring technique called *destructured assignment* to save ourselves some keystrokes. In destructured assignment we create a variable with the name of an object’s key that is wrapped in curly braces `{ }` and assign to it the object. Take a look at the example below:

```js
const { residence } = vampire; 
console.log(residence); // Prints 'Transylvania'
```

Look back at the `vampire` object’s properties in the first code example. Then, in the example above, we declare a new variable `residence` that extracts the value of the `residence` property of `vampire`. When we log the value of `residence` to the console, `'Transylvania'` is printed.

We can even use destructured assignment to grab nested properties of an object:

```js
const { day } = vampire.preferences; 
console.log(day); // Prints 'stay inside'
```

<a id="built-in-object-methods"></a>
#### Built-in Object Methods

In the previous exercises we’ve been creating instances of objects that have their own methods. But, we can also take advantage of built-in methods for Objects!

For example, we have access to object instance methods like: `.hasOwnProperty()`, `.valueOf()`, and many more! Practice your documentation reading skills and check out: [MDN’s object instance documentation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object#Methods).

There are also useful Object class methods such as `Object.assign()`, `Object.entries()`, and `Object.keys()` just to name a few. For a comprehensive list, browse: [MDN’s object instance documentation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object#Methods_of_the_Object_constructor).

Let’s get acquainted with some of these methods and their documentation.

Note: You will see errors as you work through this exercise, but by the end the errors will be fixed!

<a id="review-3"></a>
#### Review

Congratulations on finishing Advanced Objects!

Let’s review the concepts covered in this lesson:

- The object that a method belongs to is called the calling object.

- The `this` keyword refers the calling object and can be used to access properties of the calling object.

- Methods do not automatically have access to other internal properties of the calling object.

- The value of `this` depends on where the `this` is being accessed from.

- We cannot use arrow functions as methods if we want to access other internal properties.

- JavaScript objects do not have built-in privacy, rather there are conventions to follow to notify other developers about the intent of the code.

- The usage of an underscore before a property name means that the original developer did not intend for that property to be directly changed.

- Setters and getter methods allow for more detailed ways of accessing and assigning properties.

- Factory functions allow us to create object instances quickly and repeatedly.

- There are different ways to use object destructuring: one way is the property value shorthand and another is destructured assignment.

- As with any concept, it is a good skill to learn how to use the documentation with objects!

You’re ready to start leveraging more elegant code for creating and accessing objects in your code!

<a id="classes"></a>
## Classes

<a id="introduction-to-classes"></a>
### Introduction to Classes

JavaScript is an *object-oriented programming* (OOP) language we can use to model real-world items. In this lesson, you will learn how to make *classes*. Classes are a tool that developers use to quickly produce similar objects.

Take, for example, an object representing a dog named `halley`. This dog’s `name` (a key) is `"Halley"` (a value) and has an `age` (another key) of `3` (another value). We create the `halley` object below:

```js
let halley = {
  _name: 'Halley',
  _behavior: 0,

  get name() {
    return this._name;
  },

  get behavior() {
    return this._behavior;
  },

  incrementBehavior() {
    this._behavior++;
  }
}
```

Now, imagine you own a dog daycare and want to create a catalog of all the dogs who belong to the daycare. Instead of using the syntax above for every dog that joins the daycare, we can create a `Dog` class that serves as a template for creating new `Dog` objects. For each new dog, you can provide a value for their name.

As you can see, classes are a great way to reduce duplicate code and debugging time.

After we lay the foundation for classes in the first few exercises, we will introduce inheritance and static methods — two features that will make your code more efficient and meaningful.

<a id="constructor"></a>
### Constructor

In the last exercise, you created a class called `Dog`, and used it to produce a `Dog` object.

Although you may see similarities between class and object syntax, there is one important method that sets them apart. It’s called the *constructor* method. JavaScript calls the `constructor()` method every time it creates a new *instance* of a class.

```js
class Dog {
  constructor(name) {
    this.name = name;
    this.behavior = 0;
  }
}
```

- `Dog` is the name of our class. By convention, we capitalize and CamelCase class names.

- JavaScript will invoke the `constructor()` method every time we create a new instance of our `Dog` class.

- This `constructor()` method accepts one argument, `name`.

- Inside of the `constructor()` method, we use the `this` keyword. In the context of a class, `this` refers to an instance of that class. In the `Dog` class, we use `this` to set the value of the Dog instance’s `name` property to the `name` argument.

- Under `this.name`, we create a property called `behavior`, which will keep track of the number of times a dog misbehaves. The `behavior` property is always initialized to zero.

In the next exercise, you will learn how to create `Dog` instances.

<a id="instance"></a>
### Instance

Now, we’re ready to create class instances. An *instance* is an object that contains the property names and methods of a class, but with unique property values. Let’s look at our `Dog` class example.

```js
class Dog {
  constructor(name) {
    this.name = name;
    this.behavior = 0;
  } 
}

const halley = new Dog('Halley'); // Create new Dog instance
console.log(halley.name); // Log the name value saved to halley
// Output: 'Halley'
```

Below our `Dog` class, we use the `new` keyword to create an instance of our `Dog` class. Let’s consider the line of code step-by-step.

- We create a new variable named `halley` that will store an instance of our `Dog` class.

- We use the `new` keyword to generate a new instance of the `Dog` class. The `new` keyword calls the `constructor()`, runs the code inside of it, and then returns the new instance.

- We pass the `'Halley'` string to the `Dog` constructor, which sets the name property to `'Halley'`.

- Finally, we log the value saved to the name key in our halley object, which logs `'Halley'` to the console.

Now you know how to create instances. In the next exercise, you will learn how to add getters, setters, and methods.

<a id="methods-1"></a>
### Methods

At this point, we have a `Dog` class that spins up objects with `name` and `behavior` properties. Below, we will add getters and a method to bring our class to life.

Class method and getter syntax is the same as it is for objects **except you can not include commas between methods**.

```js
class Dog {
  constructor(name) {
    this._name = name;
    this._behavior = 0;
  }

  get name() {
    return this._name;
  }

  get behavior() {
    return this._behavior;
  }

  incrementBehavior() {
    this._behavior++;
  }
}
```

In the example above, we add getter methods for `name` and `behavior`. Notice, we also prepended our property names with underscores (`_name` and `_behavior`), which indicate these properties should not be accessed directly. Under the getters, we add a method named `.incrementBehavior()`. When you call `.incrementBehavior()` on a Dog instance, it adds `1` to the `_behavior` property. Between each of our methods, we did *not* include commas.

<a id="method-calls"></a>
### Method Calls

Finally, let’s use our new methods to access and manipulate data from `Dog` instances.

```js
class Dog {
  constructor(name) {
    this._name = name;
    this._behavior = 0;
  }

  get name() {
    return this._name;
  }

  get behavior() {
    return this._behavior;
  }   

  incrementBehavior() {
    this._behavior++;
  }
}

const halley = new Dog('Halley');
```

In the example above, we create the `Dog` class, then create an instance, and save it to a variable named `halley`.

The syntax for calling methods and getters on an instance is the same as calling them on an object — append the instance with a period, then the property or method name. For methods, you must also include opening and closing parentheses.

Let’s take a moment to create two `Dog` instances and call our `.incrementBehavior()` method on one of them.

```js
let nikko = new Dog('Nikko'); // Create dog named Nikko
nikko.incrementBehavior(); // Add 1 to nikko instance's behavior
let bradford = new Dog('Bradford'); // Create dog name Bradford
console.log(nikko.behavior); // Logs 1 to the console
console.log(bradford.behavior); // Logs 0 to the console
```

In the example above, we create two new `Dog` instances, `nikko` and `bradford`. Because we increment the behavior of our `nikko` instance, but not `bradford`, accessing `nikko.behavior` returns `1` and accessing `bradford.behavior` returns `0`.

<a id="inheritance-i"></a>
### Inheritance I

Imagine our doggy daycare is so successful that we decide to expand the business and open a kitty daycare. Before the daycare opens, we need to create a `Cat` class so we can quickly generate `Cat` instances. We know that the properties in our `Cat` class (`name`, `behavior`) are similar to the properties in our `Dog` class, though, there will be some differences, because of course, cats are not dogs.

Let’s say that our `Cat` class looks like this:

```js
class Cat {
  constructor(name, usesLitter) {
    this._name = name;
    this._usesLitter = usesLitter;
    this._behavior = 0;
  }

  get name() {
    return this._name;
  }

  get usesLitter() {
    return this._usesLitter;
  }

  get behavior() {
    return this._behavior;
  }  

  incrementBehavior() {
    this._behavior++;
  }
}
```

In the example above, we create a `Cat` class. It shares a couple of properties (`_name` and `_behavior`) and a method (`.incrementBehavior()`) with the `Dog` class from earlier exercises. The `Cat` class also contains one additional property (`_usesLitter`), that holds a boolean value to indicate whether a cat can use their litter box.

When multiple classes share properties or methods, they become candidates for *inheritance* — a tool developers use to decrease the amount of code they need to write.

With inheritance, you can create a *parent* class (also known as a superclass) with properties and methods that multiple *child* classes (also known as subclasses) share. The child classes inherit the properties and methods from their parent class.

Let’s abstract the shared properties and methods from our `Cat` and `Dog` classes into a parent class called `Animal`.

```js
class Animal {
  constructor(name) {
    this._name = name;
    this._behavior = 0;
  }

  get name() {
    return this._name;
  }

  get behavior() {
    return this._behavior;
  }   

  incrementBehavior() {
    this._behavior++;
  }
} 
```

In the example above, the `Animal` class contains the properties and methods that the `Cat` and `Dog` classes share (`name`, `behavior`, `.incrementBehavior()`).

The diagram to the right shows the relationships we want to create between the Animal, Cat, and Dog classes.

<a id="inheritance-ii"></a>
### Inheritance II

In the last exercise, we created a parent class named `Animal` for two child classes named `Cat` and `Dog`.

The `Animal` class below contains the shared properties and methods of `Cat` and `Dog`.

```js
class Animal {
  constructor(name) {
    this._name = name;
    this._behavior = 0;
  }

  get name() {
    return this._name;
  }

  get behavior() {
    return this._behavior;
  }   

  incrementBehavior() {
    this._behavior++;
  }
} 
```

The code below shows the `Cat` class that will inherit information from the `Animal` class.

```js
class Cat {
  constructor(name, usesLitter) {
    this._name = name;
    this._usesLitter = usesLitter;
    this._behavior = 0;
  }

  get name() {
    return this._name;
  }

  get behavior() {
    return this._behavior;
  }

  get usesLitter() {
    return this._usesLitter;
  }

  incrementBehavior() {
    this._behavior++;
  }
}
```

To the right, in **main.js**, you will put what you learned to practice by creating a parent class named `HospitalEmployee`.

<a id="inheritance-iii"></a>
### Inheritance III

We’ve abstracted the shared properties and methods of our `Cat` and `Dog` classes into a parent class called `Animal` (See below).

```js
class Animal {
  constructor(name) {
    this._name = name;
    this._behavior = 0;
  }

  get name() {
    return this._name;
  }

  get behavior() {
    return this._behavior;
  }

  incrementBehavior() {
    this._behavior++;
  }
} 
```

Now that we have these shared properties and methods in the parent `Animal` class, we can extend them to the subclass, `Cat`.

```js
class Cat extends Animal {
  constructor(name, usesLitter) {
    super(name);
    this._usesLitter = usesLitter;
  }
}
```

In the example above, we create a new class named `Cat` that extends the `Animal` class. Let’s pay special attention to our new keywords: `extends` and `super`.

- The `extends` keyword makes the methods of the animal class available inside the cat class.

- The constructor, called when you create a new `Cat` object, accepts two arguments, `name` and `usesLitter`.

- The `super` keyword calls the constructor of the parent class. In this case, `super(name)` passes the name argument of the `Cat` class to the constructor of the `Animal` class. When the `Animal` constructor runs, it sets `this._name = name;` for new `Cat` instances.

- `_usesLitter` is a new property that is unique to the `Cat` class, so we set it in the Cat constructor.

Notice, we call `super` on the first line of our `constructor()`, then set the `usesLitter` property on the second line. In a `constructor()`, you must always call the `super` method before you can use the `this` keyword — if you do not, JavaScript will throw a reference error. To avoid reference errors, it is best practice to call `super` on the first line of subclass constructors.

Below, we create a new `Cat` instance and call its name with the same syntax as we did with the `Dog` class:

```js
const bryceCat = new Cat('Bryce', false); 
console.log(bryceCat._name); // output: Bryce
```

In the example above, we create a new instance the `Cat` class, named `bryceCat`. We pass it `'Bryce'` and `false` for our `name` and `usesLitter` arguments. When we call `console.log(bryceCat._name)` our program prints, `Bryce`.

In the example above, we abandoned best practices by calling our `_name` property directly. In the next exercise, we’ll address this by calling an inherited getter method for our `name` property.

<a id="inheritance-iv"></a>
### Inheritance IV

Now that we know how to create an object that inherits properties from a parent class let’s turn our attention to methods.

When we call `extends` in a class declaration, all of the parent methods are available to the child class.

Below, we extend our `Animal` class to a `Cat` subclass.

```js
class Animal {
  constructor(name) {
    this._name = name;
    this._behavior = 0;
  }

  get name() {
    return this._name;
  }

  get behavior() {
    return this._behavior;
  }

  incrementBehavior() {
    this._behavior++;
  }
} 


class Cat extends Animal {
  constructor(name, usesLitter) {
    super(name);
    this._usesLitter = usesLitter;
  }
}

const bryceCat = new Cat('Bryce', false);
```

In the example above, our `Cat` class extends `Animal`. As a result, the `Cat` class has access to the `Animal` getters and the `.incrementBehavior()` method.

Also in the code above, we create a `Cat` instance named `bryceCat`. Because `bryceCat` has access to the `name` getter, the code below logs `'Bryce'` to the console.

```js
console.log(bryceCat.name);
```

Since the `extends` keyword brings all of the parent’s getters and methods into the child class, `bryceCat.name` accesses the `name` getter and returns the value saved to the `name` property.

Now consider a more involved example and try to answer the following question: What will the code below log to the console?

```js
bryceCat.incrementBehavior(); // Call .incrementBehavior() on Cat instance 
console.log(bryceCat.behavior); // Log value saved to behavior
```

The correct answer is `1`. But why?

- The `Cat` class inherits the `_behavior` property, `behavior` getter, and the `.incrementBehavior()` method from the `Animal` class.

- When we created the `bryceCat` instance, the `Animal` constructor set the `_behavior` property to zero.

- The first line of code calls the inherited `.incrementBehavior()` method, which increases the `bryceCat` `_behavior` value from zero to one.

- The second line of code calls the `behavior` getter and logs the value saved to `_behavior` (`1`).

<a id="inheritance-v"></a>
### Inheritance V

In addition to the inherited features, child classes can contain their own properties, getters, setters, and methods.

Below, we will add a `usesLitter` getter. The syntax for creating getters, setters, and methods is the same as it is in any other class.

```js
class Cat extends Animal {
  constructor(name, usesLitter) {
    super(name);
    this._usesLitter = usesLitter;
  }

  get usesLitter() {
    return this._usesLitter;
  }
}
```

In the example above, we create a `usesLitter` getter in the `Cat` class that returns the value saved to `_usesLitter`.

Compare the `Cat` class above to the one we created without inheritance:

```js
class Cat {
  constructor(name, usesLitter) {
    this._name = name;
    this._usesLitter = usesLitter;
    this._behavior = 0;
  }

  get name() {
    return this._name;
  }

  get usesLitter() {
    return this._usesLitter;
  }

  get behavior() {
    return this._behavior;
  }   

  incrementBehavior() {
    this._behavior++;
  }
}
```

We decreased the number of lines required to create the `Cat` class by about half. Yes, it did require an extra class (`Animal`), making the reduction in the size of our `Cat` class seem moot. However, the benefits (time saved, readability, efficiency) of inheritance grow as the number and size of your subclasses increase.

One benefit is that when you need to change a method or property that multiple classes share, you can change the parent class, instead of each subclass.

Before we move past inheritance, take a moment to see how we would create an additional subclass, called `Dog`.

```js
class Dog extends Animal {
  constructor(name) {
    super(name);
  }
}
```

This `Dog` class has access to the same properties, getters, setters, and methods as the `Dog` class we made without inheritance, and is a quarter the size.

Now that we’ve abstracted animal daycare features, it’s easy to see how you can extend `Animal` to support other classes, like `Rabbit`, `Bird` or even `Snake`.

<a id="static-methods"></a>
### Static Methods

Sometimes you will want a class to have methods that aren’t available in individual instances, but that you can call directly from the class.

Take the `Date` class, for example — you can both create `Date` instances to represent whatever date you want, and call static methods, like `Date.now()` which returns the current date, directly from the class. The `.now()` method is static, so you can call it directly from the class, but not from an instance of the class.

Let’s see how to use the `static` keyword to create a static method called `generateName` method in our `Animal` class:

```js
class Animal {
  constructor(name) {
    this._name = name;
    this._behavior = 0;
  }

  static generateName() {
    const names = ['Angel', 'Spike', 'Buffy', 'Willow', 'Tara'];
    const randomNumber = Math.floor(Math.random()*5);
    return names[randomNumber];
  }
} 
```

In the example above, we create a `static` method called `.generateName()` that returns a random name when it’s called. Because of the static keyword, we can only access `.generateName()` by appending it to the `Animal` class.

We call the `.generateName()` method with the following syntax:

```js
console.log(Animal.generateName()); // returns a name
```

You cannot access the `.generateName()` method from instances of the `Animal` class or instances of its subclasses (See below).

```js
const tyson = new Animal('Tyson'); 
tyson.generateName(); // TypeError
```

The example above will result in an error, because you cannot call static methods (`.generateName()`) on an instance (`tyson`).

<a id="review-4"></a>
### Review

Way to go! Let’s review what you learned.

- *Classes* are templates for objects.

- Javascript calls a *constructor* method when we create a new instance of a class.

- *Inheritance* is when we create a parent class with properties and methods that we can extend to child classes.

- We use the `extends` keyword to create a subclass.
The `super` keyword calls the `constructor()` of a parent class.

- Static methods are called on the class, but not on instances of the class.

In completing this lesson, you’ve taken one step closer to writing efficient, production-level JavaScript. Good luck as you continue to develop your skills and move into intermediate-level concepts.

<a id="browser-compatibility-and-transpilation"></a>
## Browser Compatibility and Transpilation

<a id="intro"></a>
### Intro

You’re probably prompted to update your web browser every few months. Do you know why? A few reasons include addressing security vulnerabilities, adding features, and supporting new HTML, CSS, and JavaScript syntax.

The reasons above imply there is a period before a software update is released when there are security vulnerabilities and unsupported language syntax.

This lesson focuses on the latter. Specifically, how developers address the gap between the new JavaScript syntax that they use and the JavaScript syntax that web browsers recognize.

This has become a widespread concern for web developers since Ecma International, the organization responsible for standardizing JavaScript, released a new version of it in 2015, called ECMAScript2015, commonly referred to as ES6. Note, the 6 refers to the version of JavaScript and is not related to the year it was released (the previous version was ES5).

Upon release, web developers quickly adopted the new ES6 syntax, as it improved readability and efficiency. However, ES6 was not supported by most web browsers, so developers ran into browser compatibility issues.

In this lesson, you will learn about two important tools for addressing browser compatibility issues.

 - [caniuse.com](caniuse.com) — A website that provides data on web browser compatibility for HTML, CSS, and JavaScript features. You will learn how to use it to look up ES6 feature support.

 - Babel — A Javascript library that you can use to convert new, unsupported JavaScript (ES6), into an older version (ES5) that is recognized by most modern browsers.

Let’s get started by running ES6 JavaScript on a fake old web browser version.

<a id="caniusecom"></a>
### caniuse.com

Since Ecma’s release of ECMAScript2015 (ES6), software companies have slowly added support for ES6 features and syntax. While most new browser versions support the majority of the ES6 library, there are still a couple sources of compatibility issues:

- Some users have not updated to the latest, ES6 supported web browser version.

- A few ES6 features, like modules, are still not supported by most web browsers.

Because companies add support for ES6 features gradually, it’s important for you to know how to look up browser support on a feature-by-feature basis. The website [caniuse.com](caniuse.com) is the best resource for finding browser compatibility information.

In caniuse, you can enter an ES6 feature, like let, and see the percentage of browsers that recognize it. You can also see when each major web browser (Chrome, Safari, Edge, etc.) added support for the keyword.

The video to the right shows you how to get started with [caniuse.com](caniuse.com).

Now, it’s your turn to get some practice with [caniuse.com](caniuse.com). In a new tab, open [caniuse.com](caniuse.com) and complete the tasks below.

<a id="why-es6"></a>
### Why ES6?

Before we learn how to set up a JavaScript project that converts ES6 to an older version, it’s worth understanding a few of the reasons Ecma made such substantial updates.

The version of JavaScript that preceded ES6 is called JavaScript ES5. Three reasons for the ES5 to ES6 updates are listed below:

- A similarity to other programming languages — JavaScript ES6 is syntactically more similar to other object-oriented programming languages. This leads to less friction when experienced, non-JavaScript developers want to learn JavaScript.

- Readability and economy of code — The new syntax is often easier to understand (more readable) and requires fewer characters to create the same functionality (economy of code).

- Addresses sources of ES5 bugs — Some ES5 syntax led to common bugs. With ES6, Ecma introduced syntax that mitigates some of the most common pitfalls.

Because ES6 addressed the above issues, Ecma knew that adoption by web developers would occur quickly, while web browser support lagged behind.

To limit the impact of ES6 browser compatibility issues, Ecma made the new syntax backwards compatible, which means you can map JavaScript ES6 code to ES5.

<a id="transplation-with-babel"></a>
### Transplation With Babel

In the last exercise, you manually converted ES6 code to ES5. Although manual conversion only took you a few minutes, it is unsustainable as the size of the JavaScript file increases.

Because ES6 is predictably backwards compatible, a collection of JavaScript programmers developed a JavaScript library called Babel that transpiles ES6 JavaScript to ES5.

Transpilation is the process of converting one programming language to another.

In the remaining exercises of this lesson, you will learn how to use Babel to transpile the new, easy-to-write version of JavaScript (ES6) to the old, browser-compatible version of JavaScript (ES5).

In the instructions below, you will pass JavaScript ES6 code to Babel, which will transpile it to ES5 and write it to a file in the lib directory.

<a id="npm-init"></a>
### npm init

In the last exercise, you wrote one command in your terminal to transpile ES6 code to ES5. In the next five exercises you will learn how to setup a JavaScript project that transpiles code when you run `npm run build` from the root directory of a JavaScript project.

The first step is to place your ES6 JavaScript file in a directory called src. From your root directory, the path to the ES6 file is `./src/main.js`

The initial JavaScript project file structure is:

```
project
|_ src
|___ main.js
```

Before we install Babel, we need to setup our project to use the [node package manager (npm)](https://docs.npmjs.com/getting-started/what-is-npm). Developers use _npm_ to access and manage Node packages. Node packages are directories that contain JavaScript code written by other developers. You can use these packages to reduce duplication of work and avoid bugs.

Before we can add Babel to our project directory, we need to run `npm init`. The `npm init` command creates a **package.json** file in the root directory.

A **package.json** file contains information about the current JavaScript project. Some of this information includes:

- Metadata — This includes a project title, description, authors, and more.

- A list of node packages required for the project — If another developer wants to run your project, npm looks inside **package.json** and downloads the packages in this list.

- Key-value pairs for command line scripts — You can use npm to run these shorthand scripts to perform some process. In a later exercise, we will add a script that runs Babel and transpiles ES6 to ES5.

If you have Node installed on your computer, you can create a **package.json** file by typing `npm init` into the terminal.

The terminal prompts you to fill in fields for the project’s metadata (name, description, etc.)

You are not required to answer the prompts, though we recommend at minimum, you add your own title and description. If you don’t want to fill in a field, you can press enter. npm will leave fill these fields with default values or leave them empty when it creates the **package.json** file.

After you run `npm init` your directory structure will contain the following files and folders:

```
project
|_ src
|___ main.js
|_ package.json
```

npm adds the **package.json** file to the same level as the src directory.

<a id="instal-node-packages"></a>
### Instal Node Packages

We use the npm `install` command to install new Node packages locally. The `install` command creates a folder called `node_modules` and copies the package files to it. The `install` command also installs all of the dependencies for the given package.

To install Babel, we need to `npm install` two packages, `babel-cli` and `babel-preset-env`. However, npm installs over one hundred other packages that are dependencies for Babel to run properly.

We install Babel with the following two commands:

```bash
$ npm install babel-cli -D
$ npm install babel-preset-env -D
```

The `babel-cli` package includes command line Babel tools, and the `babel-preset-env` package has the code that maps any JavaScript feature, ES6 and above (ES6+), to ES5.

The `-D` flag instructs npm to add each package to a property called `devDependencies` in **package.json**. Once the project’s dependencies are listed in `devDependencies`, other developers can run your project without installing each package separately. Instead, they can simply run `npm install` — it instructs npm to look inside **package.json** and download all of the packages listed in devDependencies.

Once you npm install packages, you can find the Babel packages and all their dependencies in the **node_modules** folder. The new directory structure contains the following:

```
project
|_ node_modules
|___ .bin
|___ ...
|_ src
|___ main.js
|_ package.json
```

The `...` in the file structure above is a placeholder for 100+ packages that npm installed.


<a id="babelrc"></a>
### .babelrc

Now that you’ve downloaded the Babel packages, you need to specify the version of the source JavaScript code.

You can specify the initial JavaScript version inside of a file named `.babelrc`. In your root directory, you can run `touch .babelrc` to create this file.

Your project directory contains the following folders and files:

```
project
|_ node_modules
|___ .bin
|___ ...
|_ src
|___ main.js
|_ .babelrc
|_ package.json
```

Inside `.babelrc` you need to define the *preset* for your source JavaScript file. The preset specifies the version of your initial JavaScript file.

Usually, you want to transpile JavaScript code from versions ES6 and later (ES6+) to ES5. From this point on, we will refer to our source code as ES6+, because Ecma introduces new syntax with each new version of JavaScript.

To specify that we are transpiling code from an ES6+ source, we have to add the following JavaScript object into `.babelrc`:

```json
{
  "presets": ["env"]
}
```

When you run Babel, it looks in `.babelrc` to determine the version of the initial JavaScript file. In this case, `["env"]` instructs Babel to transpile any code from versions ES6 and later.


<a id="babel-source-lib"></a>
### Babel Source Lib

There’s one last step before we can transpile our code. We need to specify a script in **package.json** that initiates the ES6+ to ES5 transpilation.

Inside of the **package.json** file, there is a property named `"scripts"` that holds an object for specifying command line shortcuts. It looks like this:

```json
...
"scripts": {
  "test": "echo \"Error: no test specified\" && exit 1"
}, ...
```

In the code above, the `"scripts"` property contains an object with one property called `"test"`. Below the `"test"` property, we will add a script that runs Babel like this:

```json
...
"scripts": {
  "test": "echo \"Error: no test specified\" && exit 1",
  "build": "babel src -d lib"
}
```

In the `"scripts"` object above, we add a property called `"build"`. The property’s value, `"babel src -d lib"`, is a command line method that transpiles ES6+ code to ES5. Let’s consider each argument in the method call:

- `babel` — The Babel command call responsible for transpiling code.

- `src` — Instructs Babel to transpile all JavaScript code inside the src directory.

- `-d` — Instructs Babel to write the transpiled code to a directory.

- `lib` — Babel writes the transpiled code to a directory called lib.

In the next exercise, we’ll run the `babel src -d lib` method to transpile our ES6+ code.

<a id="build"></a>
### Build

We’re ready to transpile our code! In the last exercise, we wrote the following script in **package.json**:

```json
"build": "babel src -d lib"
```

Now, we need to call `"build"` from the command line to transpile and write ES5 code to a directory called `lib`.

From the command line, we type:

```bash
npm run build
```

The command above runs the `build` script in **package.json**.

Babel writes the ES5 code to a file named **main.js** (it’s always the same name as the original file), inside of a folder called `lib`. The resulting directory structure is:

```
project
|_ lib
|___ main.js
|_ node_modules
|___ .bin
|___ ...
|_ src
|___ main.js
|_ .babelrc
|_ package.json
```

Notice, the directory contains a new folder named **lib**, with one file, called **main.js**.

The `npm run build` command will transpile all JavaScript files inside of the src folder. This is helpful as you build larger JavaScript projects — regardless of the number of JavaScript files, you only need to run one command (`npm run build`) to transpile all of your code.

<a id="review-5"></a>
### Review

In this lesson, you learned about browser compatibility and transpilation. Let’s review the key concepts:

- ES5 — The old JavaScript version that is supported by all modern web browsers.

- ES6 — The new(er) JavaScript version that is not supported by all modern web browsers. The syntax is more readable, similar to other programming languages, and addresses the source of common bugs in ES5.

- [caniuse.com](caniuse.com) — a website you can use to look up HTML, CSS, and JavaScript browser compatibility information.

- Babel — A JavaScript package that transpiles JavaScript ES6+ code to ES5.

- `npm init` — A terminal command that creates a **package.json** file.

- **package.json** — A file that contains information about a JavaScript project.

- `npm install` — A command that installs Node packages.
babel-cli — A Node package that contains command line tools for Babel.

- `babel-preset-env` — A Node package that contains ES6+ to ES5 syntax mapping information.

- **.babelrc** — A file that specifies the version of the JavaScript source code.

- `"build"` script — A **package.json** script that you use to tranpsile ES6+ code to ES5.

- `npm run build` — A command that runs the `build` script and transpiles ES6+ code to ES5.
For future reference, here is a list of the steps needed to set up a project for transpilation:

1. Initialize your project using `npm init` and create a directory called **src**

1. Install babel dependencies by running

```bash
npm install babel-cli -D
npm install babel-preset-env -D
```

1. Create a `.babelrc` file inside your project and add the following code inside it:

```json
{
  "presets": ["env"]
}
```

1. Add the following script to your `scripts` object in **package.json**:

```json
"build": "babel src -d lib"
```

1. Run `npm run build` whenever you want to transpile your code from your **src** to **lib** directories.

<a id="modules"></a>
## Modules

<a id="hello-modules"></a>
### Hello Modules

JavaScript *modules* are reusable pieces of code that can be exported from one program and imported for use in another program.

Modules are particularly useful for a number of reasons. By separating code with similar logic into files called modules, we can:

- find, fix, and debug code more easily;

- reuse and recycle defined logic in different parts of our application;

- keep information private and protected from other modules;

- and, importantly, prevent pollution of the global namespace and potential naming collisions, by cautiously selecting variables and behavior we load into a program.

<a id="moduleexports"></a>
### module.exports

We can get started with modules by defining a module in one file and making the module available for use in another file. Below is an example of how to define a module and how to export it using the statement `module.exports`.

In **menu.js** we write:

```js
let Menu = {};
Menu.specialty = "Roasted Beet Burger with Mint Sauce";

module.exports = Menu; 
```

Let’s consider what this code means.

1. `let Menu = {};` creates the object that represents the module `Menu`. The statement contains an uppercase variable named `Menu` which is set equal to an object.

1. `Menu.specialty` is defined as a property of the `Menu` module. We add data to the `Menu` object by setting properties on that object and giving the properties a value.

1. `"Roasted Beet Burger with Mint Sauce";` is the value stored in the `Menu.specialty` property.

1. `module.exports = Menu;` exports the `Menu` object as a module. `module` is a variable that represents the module, and `exports` exposes the module as an object.
The pattern we use to export modules is thus:

1. Define an object to represent the module.

1. Add data or behavior to the module.

1. Export the module.

1. Let’s create our first module.

<a id="require"></a>
### require()

To make use of the exported module and the behavior we define within it, we import the module. A common way to do this is with the `require()` function.

For instance, say we want the module to control the menu’s data and behavior, and we want a separate file to handle placing an order. We would create a separate file **order.js** and import the `Menu` module from menu.js to **order.js** using `require()`:

In **order.js** we would write:

```js
const Menu = require('./menu.js');

function placeOrder() {
  console.log('My order is: ' + Menu.specialty);
}

placeOrder();
```

We now have the entire behavior of `Menu` available in **order.js**. Here, we notice:

1. In **order.js** we import the module by creating a variable with const called Menu and setting it equal to the value of the require() function. We can set this variable to any variable name we like, such as menuItems.

1. require() is a JavaScript function that enables a module to load by passing in the module’s filepath as a parameter.

1. './menu.js' is the argument we pass to the require() function.

1. The placeOrder() function then uses the Menu module in its function definition. By calling Menu.specialty, we access the property specialty defined in the Menu module.

1. We can then invoke the function using placeOrder()

Taking a closer look, the pattern to import a module is:

1. Import the module

1. Use the module and its properties within a program.

<a id="moduleexports-ii"></a>
### module.exports II

We can also wrap any collection of data and functions in an object, and export the object using `module.exports`. In **menu.js**, we could write:

```js
let Menu = {};

module.exports = {
  specialty: "Roasted Beet Burger with Mint Sauce",
  getSpecialty: function() {
    return this.specialty;
  } 
}; 
```

In the above code, notice:

1. module.exports exposes the current module as an object.

1. specialty and getSpecialty are properties on the object.

Then in **order.js**, we write:

```js
const Menu = require('./menu.js');

console.log(Menu.getSpecialty());
```

Here we can still access the behavior in the `Menu` module.

<a id="export-default"></a>
### export default

As of ES6, JavaScript has implemented a new more readable and flexible syntax for exporting modules. These are usually broken down into one of two techniques, *default export* and *named exports*.

We’ll begin with the first syntax, default export. The default export syntax works similarly to the `module.exports` syntax, allowing us to export one module per file.

Let’s look at an example in **menu.js**.

```js
let Menu = {};

export default Menu;
```

1. `export default` uses the JavaScript `export` statement to export JavaScript objects, functions, and primitive data types.

1. `Menu` refers to the name of the `Menu` object, the object that we are setting the properties on within our modules.

When using ES6 syntax, we use `export default` in place of `module.exports`.

<a id="import"></a>
### import

ES6 module syntax also introduces the `import` keyword for importing objects. In our **order.js** example, we import an object like this:

```js
import Menu from './menu';
```

1. The `import` keyword begins the statement.

1. The keyword `Menu` here specifies the name of the variable to store the default export in.

1. `from` specifies where to load the module from.

1. `'./menu'` is the name of the module to load. When dealing with local files, it specifically refers to the name of the file without the extension of the file.

We can then continue using the 1Menu1 module in the **order.js** file.

<a id="named-exports"></a>
### Named Exports

ES6 introduced a second common approach to export modules. In addition to default export, *named exports* allow us to export data through the use of variables.

Let’s see how this works. In **menu.js** we would be sure to give each piece of data a distinct variable name:

```js
let specialty = '';
function isVegetarian() {
}; 
function isLowSodium() {
}; 

export { specialty, isVegetarian };
```

1. Notice that, when we use named exports, we are not setting the properties on an object. Each export is stored in its own variable.

1. `specialty` is a string object, while `isVegetarian` and `isLowSodium` are objects in the form of functions. Recall that in JavaScript, every function is in fact a function object.

1. `export { specialty, isVegetarian };` exports objects by their variable names. Notice the keyword `export` is the prefix.

1. `specialty` and `isVegetarian` are exported, while `isLowSodium` is not exported, since it is not specified.

<a id="named-imports"></a>
### Named Imports

To import objects stored in a variable, we use the `import` keyword and include the variables in a set of `{}`.

In the **order.js** file, for example, we would write:

```js
import { specialty, isVegetarian } from './menu';

console.log(specialty);
```

1. Here `specialty` and `isVegetarian` are imported.

1. If we did not want to import either of these variables, we could omit them from the `import` statement.

1. We can then use these objects as in within our code. For example, we would use `specialty` instead of `Menu.specialty`.

<a id="export-named-exports"></a>
### Export Named Exports

Named exports are also distinct in that they can be exported as soon as they are declared, by placing the keyword `export` in front of variable declarations.

In **menu.js**

```js
export let specialty = '';
export function isVegetarian() {
}; 
function isLowSodium() {
}; 
```

1. The `export` keyword allows us to export objects upon declaration, as shown in `export let specialty` and `export function isVegetarian() {}`.

1. We no longer need an `export` statement at the bottom of our file, since this behavior is handled above.

<a id="import-named-imports"></a>
### Import Named Imports

To import variables that are declared, we simply use the original syntax that describes the variable name. In other words, exporting upon declaration does not have an impact on how we import the variables.

```js
import { specialty, isVegetarian } from 'menu';
```

<a id="export-as"></a>
### Export as

Named exports also conveniently offer a way to change the name of variables when we export or import them. We can do this with the `as` keyword.

Let’s see how this works. In our **menu.js** example

```js
let specialty = '';
let isVegetarian = function() {
}; 
let isLowSodium = function() {
}; 

export { specialty as chefsSpecial, isVegetarian as isVeg, isLowSodium };
```

In the above example, take a look at the `export` statement at the bottom of the of the file.

1. The `as` keyword allows us to give a variable name an alias as demonstrated in `specialty as chefsSpecial` and `isVegetarian as isVeg`.

1. Since we did not give `isLowSodium` an alias, it will maintain its original name.

<a id="import-as"></a>
### Import as

To import named export aliases with the `as` keyword, we add the aliased variable in our import statement.

```js
import { chefsSpecial, isVeg } from './menu';
```

In **orders.js**

1. We import `chefsSpecial` and `isVeg` from the `Menu` object.

1. Here, note that we have an option to alias an object that was not previously aliased when exported. For example, the `isLowSodium` object that we exported could be aliased with the `as` keyword when imported: `import {isLowSodium as saltFree} from 'Menu';`

Another way of using aliases is to import the entire module as an alias:

```js
import * as Carte from './menu';

Carte.chefsSpecial;
Carte.isVeg();
Carte.isLowSodium(); 
```

1. This allows us to import an entire module from **menu.js** as an alias `Carte`.

1. In this example, whatever name we exported would be available to us as properties of that module. For example, if we exported the aliases `chefsSpecial` and `isVeg`, these would be available to us. If we did not give an alias to `isLowSodium`, we would call it as defined on the `Carte` module.

<a id="combining-export-statements"></a>
### Combining Export Statements

We can also use named exports and default exports together. In **menu.js**:

```js
let specialty = '';
function isVegetarian() {
}; 
function isLowSodium() {
}; 
function isGlutenFree() {
};

export { specialty as chefsSpecial, isVegetarian as isVeg };
export default isGlutenFree;
```

Here we use the keyword `export` to export the named exports at the bottom of the file. Meanwhile, we export the `isGlutenFree` variable using the `export default` syntax.

This would also work if we exported most of the variables as declared and exported others with the `export default` syntax.

```js
export let Menu = {};

export let specialty = '';
export let isVegetarian = function() {
}; 
export let isLowSodium = function() {
}; 
let isGlutenFree = function() {
};

export default isGlutenFree;
```

Here we use the `export` keyword to export the variables upon declaration, and again export the `isGlutenFree` variable using the `export default` syntax

While it’s better to avoid combining two methods of exporting, it is useful on occasion. For example, if you suspect developers may only be interested in importing a specific function and won’t need to import the entire default export.

<a id="combining-import-statements"></a>
### Combining Import Statements

We can import the collection of objects and functions with the same data.

We can use an `import` keyword to import both types of variables as such:

```js
import { specialty, isVegetarian, isLowSodium } from './menu';

import GlutenFree from './menu';
```

<a id="review-6"></a>
### Review

We just learned how to use JavaScript modules. Let’s review what we learned:

*Modules* in JavaScript are reusable pieces of code that can be exported from one program and imported for use in another program.

- `module.exports` exports the module for use in another program.

- `require()` imports the module for use in the current program.

ES6 introduced a more flexible, easier syntax to export modules:

- default exports use `export default` to export JavaScript objects, functions, and primitive data types.

- named exports use the `export` keyword to export data in variables.

- named exports can be aliased with the `as` keyword.

- `import` is a keyword that imports any object, function, or data type.

<a id="javascript-promises"></a>
## JavaScript Promises

<a id="introduction-2"></a>
### Introduction

In web development, asynchronous programming is notorious for being a challenging topic.

An *asynchronous* operation is one that allows the computer to “move on” to other tasks while waiting for the asynchronous operation to complete. Asynchronous programming means that time-consuming operations don’t have to bring everything else in our programs to a halt.

There are countless examples of asynchronicity in our everyday lives. Cleaning our house, for example, involves asynchronous operations such as a dishwasher washing our dishes or a washing machine washing our clothes. While we wait on the completion of those operations, we’re free to do other chores.

Similarly, web development makes use of asynchronous operations. Operations like making a network request or querying a database can be time-consuming, but JavaScript allows us to execute other tasks while awaiting their completion.

This lesson will teach you how modern JavaScript handles asynchronicity using the `Promise` object, introduced with ES6. Let’s get started!

<a id="what-is-a-promise"></a>
### What is a Promise?

Promises are objects that represent the eventual outcome of an asynchronous operation. A `Promise` object can be in one of three states:

- **Pending**: The initial state— the operation has not completed yet.

- **Fulfilled**: The operation has completed successfully and the promise now has a resolved value. For example, a request’s promise might resolve with a JSON object as its value.

- **Rejected**: The operation has failed and the promise has a reason for the failure. This reason is usually an Error of some kind.

We refer to a promise as *settled* if it is no longer pending— it is either fulfilled or rejected. Let’s think of a dishwasher as having the states of a promise:

- **Pending**: The dishwasher is running but has not completed the washing cycle.

- **Fulfilled**: The dishwasher has completed the washing cycle and is full of clean dishes.

- **Rejected**: The dishwasher encountered a problem (it didn’t receive soap!) and returns unclean dishes.

If our dishwashing promise is fulfilled, we’ll be able to perform related tasks, such as unloading the clean dishes from the dishwasher. If it’s rejected, we can take alternate steps, such as running it again with soap or washing the dishes by hand.

All promises eventually settle, enabling us to write logic for what to do if the promise fulfills or if it rejects.

<a id="constructing-a-promise-object"></a>
### Constructing a Promise Object

Let’s construct a promise! To create a new `Promise` object, we use the `new` keyword and the `Promise` constructor method:

```js
const executorFunction = (resolve, reject) => { };
const myFirstPromise = new Promise(executorFunction);
```

The `Promise` constructor method takes a function parameter called the *executor function* which runs automatically when the constructor is called. The executor function generally starts an asynchronous operation and dictates how the promise should be settled.

The executor function has two function parameters, usually referred to as the `resolve()` and `reject()` functions. The `resolve()` and `reject()` functions aren’t defined by the programmer. When the `Promise` constructor runs, JavaScript will pass **its own** `resolve()` and `reject()` functions into the executor function.

- `resolve` is a function with one argument. Under the hood, if invoked, `resolve()` will change the promise’s status from `pending` to `fulfilled`, and the promise’s resolved value will be set to the argument passed into `resolve()`.

- `reject` is a function that takes a reason or error as an argument. Under the hood, if invoked, `reject()` will change the promise’s status from `pending` to `rejected`, and the promise’s rejection reason will be set to the argument passed into `reject()`.

Let’s look at an example executor function in a `Promise` constructor:

```js
const executorFunction = (resolve, reject) => {
  if (someCondition) {
      resolve('I resolved!');
  } else {
      reject('I rejected!'); 
  }
}
const myFirstPromise = new Promise(executorFunction);
```

Let’s break down what’s happening above:

- We declare a variable `myFirstPromise`

- `myFirstPromise` is constructed using `new Promise()` which is the `Promise` constructor method.

- `executorFunction()` is passed to the constructor and has two functions as parameters: `resolve` and `reject`.

- If `someCondition` evaluates to `true`, we invoke `resolve()` with the string `'I resolved!'`

- If not, we invoke `reject()` with the string `'I rejected!'`

In our example, `myFirstPromise` resolves or rejects based on a simple condition, but, in practice, promises settle based on the results of asynchronous operations. For example, a database request may fulfill with the data from a query or reject with an error thrown. In this exercise, we’ll construct promises which resolve synchronously to more easily understand how they work.

<a id="the-node-settimeout-function"></a>
### The Node setTimeout() Function

Knowing how to construct a promise is useful, but most of the time, knowing how to *consume*, or use, promises will be key. Rather than constructing promises, you’ll be handling `Promise` objects returned to you as the result of an asynchronous operation. These promises will start off pending but settle eventually.

Moving forward, we’ll be simulating this by providing you with functions that return promises which settle after some time. To accomplish this, we’ll be using `setTimeout()`. `setTimeout()` is a Node API (a comparable API is provided by web browsers) that uses callback functions to schedule tasks to be performed after a delay. `setTimeout()` has two parameters: a callback function and a delay in milliseconds.

```js
const delayedHello = () => {
  console.log('Hi! This is an asynchronous greeting!');
};

setTimeout(delayedHello, 2000);
```

Here, we invoke `setTimeout()` with the callback function `delayedHello()` and `2000`. In at least two seconds `delayedHello()` will be invoked. But why is it “at least” two seconds and not exactly two seconds?

This delay is performed asynchronously—the rest of our program won’t stop executing during the delay. Asynchronous JavaScript uses something called the event-loop. After two seconds, `delayedHello()` is added to a line of code waiting to be run. Before it can run, any synchronous code from the program will run. Next, any code in front of it in the line will run. This means it might be more than two seconds before `delayedHello()` is actually executed.

Let’s look at how we’ll be using setTimeout() to construct asynchronous promises:

```js
const returnPromiseFunction = () => {
  return new Promise((resolve, reject) => {
    setTimeout(( ) => {resolve('I resolved!')}, 1000);
  });
};

const prom = returnPromiseFunction();
```

In the example code, we invoked `returnPromiseFunction()` which returned a promise. We assigned that promise to the variable `prom`. Similar to the asynchronous promises you may encounter in production, `prom` will initially have a status of pending.

Let’s explore `setTimeout()` a bit more.

<a id="consuming-promises"></a>
### Consuming Promises

The initial state of an asynchronous promise is `pending`, but we have a guarantee that it will settle. How do we tell the computer what should happen then? Promise objects come with an aptly named `.then()` method. It allows us to say, “I have a promise, when it settles, **then** here’s what I want to happen…”

In the case of our dishwasher promise, the dishwasher will run **then**:

- If our promise rejects, this means we have dirty dishes, and we’ll add soap and run the dishwasher again.

- If our promise fulfills, this means we have clean dishes, and we’ll put the dishes away.

`.then()` is a higher-order function— it takes two callback functions as arguments. We refer to these callbacks as handlers. When the promise settles, the appropriate handler will be invoked with that settled value.

- The first handler, sometimes called `onFulfilled`, is a *success handler*, and it should contain the logic for the promise resolving.

- The second handler, sometimes called `onRejected`, is a *failure handler*, and it should contain the logic for the promise rejecting.

We can invoke `.then()` with one, both, or neither handler! This allows for flexibility, but it can also make for tricky debugging. If the appropriate handler is not provided, instead of throwing an error, `.then()` will just return a promise with the same settled value as the promise it was called on. One important feature of `.then()` is that it always returns a promise. We’ll return to this in more detail in a later exercise and explore why it’s so important.

<a id="the-onfulfilled-and-onrejected-functions"></a>
### The onFulfilled and onRejected Functions

To handle a “successful” promise, or a promise that resolved, we invoke `.then()` on the promise, passing in a success handler callback function:

```js
const prom = new Promise((resolve, reject) => {
  resolve('Yay!');
});

const handleSuccess = (resolvedValue) => {
  console.log(resolvedValue);
};

prom.then(handleSuccess); // Prints: 'Yay!'
```

Let’s break down what’s happening in the example code:

- `prom` is a promise which will resolve to `'Yay!'`.

- We define a function, `handleSuccess()`, which prints the argument passed to it.

- We invoke `prom`‘s `.then()` function passing in our `handleSuccess()` function.

- Since `prom` resolves, `handleSuccess()` is invoked with `prom`‘s resolved value, `'Yay'`, so `'Yay'` is logged to the console.

With typical promise consumption, we won’t know whether a promise will resolve or reject, so we’ll need to provide the logic for either case. We can pass both an `onFulfilled` and `onRejected` callback to `.then()`.

```js
let prom = new Promise((resolve, reject) => {
  let num = Math.random();
  if (num < .5 ){
    resolve('Yay!');
  } else {
    reject('Ohhh noooo!');
  }
});

const handleSuccess = (resolvedValue) => {
  console.log(resolvedValue);
};

const handleFailure = (rejectionReason) => {
  console.log(rejectionReason);
};

prom.then(handleSuccess, handleFailure);
```

Let’s break down what’s happening in the example code:

- `prom` is a promise which will randomly either resolve with `'Yay!'` or reject with `'Ohhh noooo!'`.

- We pass two handler functions to `.then()`. The first will be invoked with `'Yay!'` if the promise resolves, and the second will be invoked with `'Ohhh noooo!'` if the promise rejects.

Let’s write some `onFulfilled` and `onRejected` functions!

<a id="using-catch-with-promises"></a>
### Using catch() with Promises

One way to write cleaner code is to follow a principle called *separation of concerns*. Separation of concerns means organizing code into distinct sections each handling a specific task. It enables us to quickly navigate our code and know where to look if something isn’t working.

Remember, `.then()` will return a promise with the same settled value as the promise it was called on if no appropriate handler was provided. This implementation allows us to separate our resolved logic from our rejected logic. Instead of passing both handlers into one `.then()`, we can chain a second `.then()` with a failure handler to a first `.then()` with a success handler and both cases will be handled.

```js
prom
  .then((resolvedValue) => {
    console.log(resolvedValue);
  })
  .then(null, (rejectionReason) => {
    console.log(rejectionReason);
  });
```

Since JavaScript doesn’t mind whitespace, we follow a common convention of putting each part of this chain on a new line to make it easier to read. To create even more readable code, we can use a different promise function: `.catch()`.

The `.catch()` function takes only one argument, `onRejected`. In the case of a rejected promise, this failure handler will be invoked with the reason for rejection. Using `.catch()` accomplishes the same thing as using a `.then()` with only a failure handler.

Let’s look an example using `.catch()`:

```js
prom
  .then((resolvedValue) => {
    console.log(resolvedValue);
  })
  .catch((rejectionReason) => {
    console.log(rejectionReason);
  });
```

Let’s break down what’s happening in the example code:

- `prom` is a promise which randomly either resolves with `'Yay!'` or rejects with `'Ohhh noooo!'`.

- We pass a success handler to `.then()` and a failure handler to `.catch()`.

- If the promise resolves, `.then()`‘s success handler will be invoked with `'Yay!'`.

- If the promise rejects, `.then()` will return a promise with the same rejection reason as the original promise and `.catch()`‘s failure handler will be invoked with that rejection reason.

Let’s practice writing `.catch()` functions.

<a id="chaining-multiple-promises"></a>
### Chaining Multiple Promises

One common pattern we’ll see with asynchronous programming is multiple operations which depend on each other to execute or that must be executed in a certain order. We might make one request to a database and use the data returned to us to make another request and so on! Let’s illustrate this with another cleaning example, washing clothes:

We take our dirty clothes and put them in the washing machine. If the clothes are cleaned, **then** we’ll want to put them in the dryer. After the dryer runs, if the clothes are dry, **then** we can fold them and put them away.

This process of chaining promises together is called **composition**. Promises are designed with composition in mind! Here’s a simple promise chain in code:

```js
firstPromiseFunction()
.then((firstResolveVal) => {
  return secondPromiseFunction(firstResolveVal);
})
.then((secondResolveVal) => {
  console.log(secondResolveVal);
});
```

Let’s break down what’s happening in the example:

- We invoke a function `firstPromiseFunction()` which returns a promise.

- We invoke `.then()` with an anonymous function as the success handler.

- Inside the success handler we **return** a new promise— the result of invoking a second function, `secondPromiseFunction()` with the first promise’s resolved value.

- We invoke a second `.then()` to handle the logic for the second promise settling.

- Inside that `.then()`, we have a success handler which will log the second promise’s resolved value to the console.

In order for our chain to work properly, we had to `return` the promise `secondPromiseFunction(firstResolveVal)`. This ensured that the return value of the first `.then()` was our second promise rather than the default return of a new promise with the same settled value as the initial.

Let’s write some promise chains!

<a id="avoiding-common-mistakes"></a>
### Avoiding Common Mistakes

Promise composition allows for much more readable code than the nested callback syntax that preceded it. However, it can still be easy to make mistakes. In this exercise, we’ll go over two common mistakes with promise composition.

**Mistake 1**: Nesting promises instead of chaining them.

```js
returnsFirstPromise()
.then((firstResolveVal) => {
  return returnsSecondValue(firstResolveVal)
    .then((secondResolveVal) => {
      console.log(secondResolveVal);
    })
})
```

Let’s break down what’s happening in the above code:

- We invoke `returnsFirstPromise()` which returns a promise.

- We invoke `.then()` with a success handler.

- Inside the success handler, we invoke `returnsSecondValue()` with `firstResolveVal` which will return a new promise.

- We invoke a second `.then()` to handle the logic for the second promise settling all inside the first `then()`!

- Inside that second `.then()`, we have a success handler which will log the second promise’s resolved value to the console.

Instead of having a clean chain of promises, we’ve nested the logic for one inside the logic of the other. Imagine if we were handling five or ten promises!

**Mistake 2**: Forgetting to `return` a promise.

```js
returnsFirstPromise()
.then((firstResolveVal) => {
  returnsSecondValue(firstResolveVal)
})
.then((someVal) => {
  console.log(someVal);
})
```

Let’s break down what’s happening in the example:

- We invoke `returnsFirstPromise()` which returns a promise.

- We invoke `.then()` with a success handler.

- Inside the success handler, we create our second promise, but we forget to `return` it!

- We invoke a second `.then()`. It’s supposed to handle the logic for the second promise, but since we didn’t return, this `.then()` is invoked on a promise with the same settled value as the original promise!

Since forgetting to return our promise won’t throw an error, this can be a really tricky thing to debug!

<a id="using-promiseall"></a>
### Using Promise.all()

When done correctly, promise composition is a great way to handle situations where asynchronous operations depend on each other or execution order matters. What if we’re dealing with multiple promises, but we don’t care about the order? Let’s think in terms of cleaning again.

For us to consider our house clean, we need our clothes to dry, our trash bins emptied, and the dishwasher to run. We need **all** of these tasks to complete but not in any particular order. Furthermore, since they’re all getting done asynchronously, they should really all be happening at the same time!

To maximize efficiency we should use *concurrency*, multiple asynchronous operations happening together. With promises, we can do this with the function `Promise.all()`.

`Promise.all()` accepts an array of promises as its argument and returns a single promise. That single promise will settle in one of two ways:

- If every promise in the argument array resolves, the single promise returned from `Promise.all()` will resolve with an array containing the resolve value from each promise in the argument array.

- If any promise from the argument array rejects, the single promise returned from `Promise.all()` will immediately reject with the reason that promise rejected. This behavior is sometimes referred to as *failing* fast.

Let’s look at a code example:

```js
let myPromises = Promise.all([returnsPromOne(), returnsPromTwo(), returnsPromThree()]);

myPromises
  .then((arrayOfValues) => {
    console.log(arrayOfValues);
  })
  .catch((rejectionReason) => {
    console.log(rejectionReason);
  });
```

Let’s break down what’s happening:

- We declare `myPromises` assigned to invoking `Promise.all()`.

- We invoke `Promise.all()` with an array of three promises — the returned values from functions.

- We invoke `.then()` with a success handler which will print the array of resolved values if each promise resolves successfully.

- We invoke `.catch()` with a failure handler which will print the first rejection message if any promise rejects.

<a id="review-7"></a>
### Review

Awesome job! Promises are a difficult concept even for experienced developers, so pat yourself on the back. You’ve learned a ton about asynchronous JavaScript and promises. Let’s review:

- Promises are JavaScript objects that represent the eventual result of an asynchronous operation.

- Promises can be in one of three states: pending, resolved, or rejected.

- A promise is settled if it is either resolved or rejected.

- We construct a promise by using the `new` keyword and passing an executor function to the `Promise` constructor method.

- `setTimeout()` is a Node function which delays the execution of a callback function using the event-loop.

- We use `.then()` with a success handler callback containing the logic for what should happen if a promise resolves.

- We use `.catch()` with a failure handler callback containing the logic for what should happen if a promise rejects.

- Promise composition enables us to write complex, asynchronous code that’s still readable. We do this by chaining multiple `.then()`‘s and `.catch()`‘s.

- To use promise composition correctly, we have to remember to `return` promises constructed within a `.then()`.

- We should chain multiple promises rather than nesting them.

- To take advantage of concurrency, we can use `Promise.all()`.

<a id="javascript-async-await"></a>
## JavaScript Async-Await

<a id="introduction-3"></a>
### Introduction

Often in web development, we need to handle asynchronous actions— actions we can wait on while moving on to other tasks. We make requests to networks, databases, or any number of similar operations. JavaScript is non-blocking: instead of stopping the execution of code while it waits, JavaScript uses an [event-loop](https://youtu.be/8aGhZQkoFbQ) which allows it to efficiently execute other tasks while it awaits the completion of these asynchronous actions.

Originally, JavaScript used callback functions to handle asynchronous actions. The problem with callbacks is that they encourage complexly nested code which quickly becomes difficult to read, debug, and scale. With ES6, JavaScript integrated native [promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) which allow us to write significantly more readable code. JavaScript is continually improving, and ES8 provides a new syntax for handling our asynchronous action, `async...await`. The `async...await` syntax allows us to write asynchronous code that reads similarly to traditional synchronous, imperative programs.

The `async...await` syntax is [syntactic sugar](https://en.wikipedia.org/wiki/Syntactic_sugar) — it doesn’t introduce new functionality into the language, but rather introduces a new syntax for using promises and [generators](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Generator). Both of these were already built in to the language. Despite this, `async...await` powerfully improves the readability and scalability of our code. Let’s learn how to use it!

<a id="the-async-keyword"></a>
### The async Keyword

The `async` keyword is used to write functions that handle asynchronous actions. We wrap our asynchronous logic inside a function prepended with the `async` keyword. Then, we invoke that function.

```js
async function myFunc() {
  // Function body here
};

myFunc();
```

We’ll be using `async` function declarations throughout this lesson, but we can also create `async` function expressions:

```js
const myFunc = async () => {
  // Function body here
};

myFunc();
```

`async` functions always return a promise. This means we can use traditional promise syntax, like `.then()` and `.catch` with our `async` functions. An `async` function will return in one of three ways:

- If there’s nothing returned from the function, it will return a promise with a resolved value of `undefined`.

- If there’s a non-promise value returned from the function, it will return a promise resolved to that value.

- If a promise is returned from the function, it will simply return that promise

```js
async function fivePromise() { 
  return 5;
}

fivePromise()
.then(resolvedValue => {
    console.log(resolvedValue);
  })  // Prints 5
```

In the example above, even though we return `5` inside the function body, what’s actually returned when we invoke `fivePromise()` is a promise with a resolved value of `5`.

Let’s write an `async` function!

<a id="the-await-operator"></a>
### The await Operator

In the last exercise, we covered the `async` keyword. By itself, it doesn’t do much; `async` functions are almost always used with the additional keyword `await` inside the function body.

The `await` keyword can only be used inside an `async` function. `await` is an operator: it returns the resolved value of a promise. Since promises resolve in an indeterminate amount of time, `await` halts, or pauses, the execution of our `async` function until a given promise is resolved.

In most situations, we’re dealing with promises that were returned from functions. Generally, these functions are through a library, and, in this lesson, we’ll be providing them. We can `await` the resolution of the promise it returns inside an `async` function. In the example below, `myPromise()` is a function that returns a promise which will resolve to the string `"I am resolved now!"`.

```js
async function asyncFuncExample(){
  let resolvedValue = await myPromise();
  console.log(resolvedValue);
}

asyncFuncExample(); // Prints: I am resolved now!
```

Within our `async` function, `asyncFuncExample()`, we use `await` to halt our execution until `myPromise()` is resolved and assign its resolved value to the variable `resolvedValue`. Then we log `resolvedValue` to the console. We’re able to handle the logic for a promise in a way that reads like synchronous code.

<a id="writing-async-functions"></a>
### Writing async Functions

We’ve seen that the `await` keyword halts the execution of an `async` function until a promise is no longer pending. Don’t forget the `await` keyword! It may seem obvious, but this can be a tricky mistake to catch because our function will still run— it just won’t have the desired results.

We’re going to explore this using the following function, which returns a promise that resolves to `'Yay, I resolved!'` after a 1 second delay:

```js
let myPromise = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Yay, I resolved!')
    }, 1000);
  });
}
```

Now we’ll write two `async` functions which invoke `myPromise()`:

```js
async function noAwait() {
 let value = myPromise();
 console.log(value);
}

async function yesAwait() {
 let value = await myPromise();
 console.log(value);
}

noAwait(); // Prints: Promise { <pending> }
yesAwait(); // Prints: Yay, I resolved!
```

In the first `async` function, `noAwait()`, we left off the `await` keyword before `myPromise()`. In the second, `yesAwait()`, we included it. The `noAwait()` function logs `Promise { <pending> }` to the console. Without the `await` keyword, the function execution wasn’t paused. The `console.log()` on the following line was executed before the promise had resolved.

Remember that the `await` operator returns the resolved value of a promise. When used properly in `yesAwait()`, the variable value was assigned the resolved value of the `myPromise()` promise, whereas in `noAwait()`, `value` was assigned the promise object itself.

<a id="handling-dependent-promises"></a>
### Handling Dependent Promises

The true beauty of `async...await` is when we have a series of asynchronous actions which depend on one another. For example, we may make a network request based on a query to a database. In that case, we would need to wait to make the network request until we had the results from the database. With native promise syntax, we use a chain of `.then()` functions making sure to return correctly each one:

```js
function nativePromiseVersion() {
    returnsFirstPromise()
    .then((firstValue) => {
        console.log(firstValue);
        return returnsSecondPromise(firstValue);
    })
   .then((secondValue) => {
        console.log(secondValue);
    });
}
```

Let’s break down what’s happening in the `nativePromiseVersion()` function:

- Within our function we use two functions which return promises: `returnsFirstPromise()` and `returnsSecondPromise()`.

- We invoke `returnsFirstPromise()` and ensure that the first promise resolved by using `.then()`.

- In the callback of our first `.then()`, we log the resolved value of the first promise, `firstValue`, and then return `returnsSecondPromise(firstValue)`.

- We use another `.then()` to print the second promise’s resolved value to the console.

Here’s how we’d write an `async` function to accomplish the same thing:

```js
async function asyncAwaitVersion() {
 let firstValue = await returnsFirstPromise();
 console.log(firstValue);
 let secondValue = await returnsSecondPromise(firstValue);
 console.log(secondValue);
}
```

Let’s break down what’s happening in our `asyncAwaitVersion()` function:

- We mark our function as `async`.

- Inside our function, we create a variable `firstValue` assigned `await returnsFirstPromise()`. This means `firstValue` is assigned the resolved value of the awaited promise.

- Next, we log `firstValue` to the console.

- Then, we create a variable `secondValue` assigned to `await returnsSecondPromise(firstValue)`. Therefore, `secondValue` is assigned this promise’s resolved value.

- Finally, we log `secondValue` to the console.

Though using the `async...await` syntax can save us some typing, the length reduction isn’t the main point. Given the two versions of the function, the `async...await` version more closely resembles synchronous code, which helps developers maintain and debug their code. The `async...await` syntax also makes it easy to store and refer to resolved values from promises further back in our chain which is a much more difficult task with native promise syntax. Let’s create some `async` functions with multiple `await` statements!

<a id="handling-errors"></a>
### Handling Errors

When `.catch()` is used with a long promise chain, there is no indication of where in the chain the error was thrown. This can make debugging challenging.

With `async...await`, we use `try...catch` statements for error handling. By using this syntax, not only are we able to handle errors in the same way we do with synchronous code, but we can also catch both synchronous and asynchronous errors. This makes for easier debugging!

```js
async function usingTryCatch() {
 try {
   let resolveValue = await asyncFunction('thing that will fail');
   let secondValue = await secondAsyncFunction(resolveValue);
 } catch (err) {
   // Catches any errors in the try block
   console.log(err);
 }
}

usingTryCatch();
```

Remember, since `async` functions return promises we can still use native promise’s `.catch()` with an `async` function

```js
async function usingPromiseCatch() {
   let resolveValue = await asyncFunction('thing that will fail');
}

let rejectedPromise = usingPromiseCatch();
rejectedPromise.catch((rejectValue) => {
console.log(rejectValue);
})
```

This is sometimes used in the global scope to catch final errors in complex code.

<a id="handling-independent-promises"></a>
### Handling Independent Promises

Remember that `await` halts the execution of our `async` function. This allows us to conveniently write synchronous-style code to handle dependent promises. But what if our `async` function contains multiple promises which are not dependent on the results of one another to execute?

```js
async function waiting() {
 const firstValue = await firstAsyncThing();
 const secondValue = await secondAsyncThing();
 console.log(firstValue, secondValue);
}

async function concurrent() {
 const firstPromise = firstAsyncThing();
 const secondPromise = secondAsyncThing();
console.log(await firstPromise, await secondPromise);
}
```

In the `waiting()` function, we pause our function until the first promise resolves, then we construct the second promise. Once that resolves, we print both resolved values to the console.

In our `concurrent()` function, both promises are constructed without using `await`. We then `await` each of their resolutions to print them to the console.

With our `concurrent()` function both promises’ asynchronous operations can be run simultaneously. If possible, we want to get started on each asynchronous operation as soon as possible! Within our `async` functions we should still take advantage of *concurrency*, the ability to perform asynchronous actions at the same time.

Note: if we have multiple truly independent promises that we would like to execute fully in parallel, we must use individual `.then()` functions and avoid halting our execution with `await`.

<a id="await-promiseall"></a>
### Await Promise.all()

Another way to take advantage of concurrency when we have multiple promises which can be executed simultaneously is to `await` a `Promise.all()`.

We can pass an array of promises as the argument to `Promise.all()`, and it will return a single promise. This promise will resolve when all of the promises in the argument array have resolved. This promise’s resolve value will be an array containing the resolved values of each promise from the argument array.

```js
async function asyncPromAll() {
  const resultArray = await Promise.all([asyncTask1(), asyncTask2(), asyncTask3(), asyncTask4()]);
  for (let i = 0; i<resultArray.length; i++){
    console.log(resultArray[i]); 
  }
}
```

In our above example, we `await` the resolution of a `Promise.all()`. This `Promise.all()` was invoked with an argument array containing four promises (returned from required-in functions). Next, we loop through our `resultArray`, and log each item to the console. The first element in `resultArray` is the resolved value of the `asyncTask1()` promise, the second is the value of the `asyncTask2()` promise, and so on.

`Promise.all()` allows us to take advantage of asynchronicity — each of the four asynchronous tasks can process concurrently. `Promise.all()` also has the benefit of *failing fast*, meaning it won’t wait for the rest of the asynchronous actions to complete once any one has rejected. As soon as the first promise in the array rejects, the promise returned from `Promise.all()` will reject with that reason. As it was when working with native promises, `Promise.all()` is a good choice if multiple asynchronous tasks are all required, but none must wait for any other before executing.

<a id="review-8"></a>
### Review

Awesome work getting the hang of the `async...await` syntax! Let’s review what you’ve learned:

- `async...await` is syntactic sugar built on native JavaScript promises and generators.

- We declare an async function with the keyword `async`.

- Inside an `async` function we use the `await` operator to pause execution of our function until an asynchronous action completes and the awaited promise is no longer pending .

- `await` returns the resolved value of the awaited promise.

- We can write multiple `await` statements to produce code that reads like synchronous code.

- We use `try...catch` statements within our `async` functions for error handling.

- We should still take advantage of concurrency by writing `async` functions that allow asynchronous actions to happen in concurrently whenever possible.

<a id="requests"></a>
## Requests
